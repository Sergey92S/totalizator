package com.shmouradko.totalizator;

import com.shmouradko.totalizator.service.impl.CompetitionService;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class CompetitionServiceTest extends TestCase {

    @Test
    public void test() {
        CompetitionService competitionService = CompetitionService.getInstance();
        assertTrue(competitionService.getAllGames() != null);
    }

}
