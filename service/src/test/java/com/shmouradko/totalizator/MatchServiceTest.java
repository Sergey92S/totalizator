package com.shmouradko.totalizator;

import com.shmouradko.totalizator.service.impl.MatchService;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class MatchServiceTest extends TestCase {

    @Test
    public void test() {
        MatchService matchService = MatchService.getInstance();
        assertTrue(matchService.defineAllMatches(1) != null);
        assertTrue(matchService.defineAllMatchInfo(1) != null);
        assertTrue(matchService.defineMatchInfo(1) != null);
    }

}