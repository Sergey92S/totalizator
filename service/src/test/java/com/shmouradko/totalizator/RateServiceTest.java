package com.shmouradko.totalizator;

import com.shmouradko.totalizator.service.impl.RateService;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class RateServiceTest extends TestCase {

    @Test
    public void test() {
        RateService rateService = RateService.getInstance();
        assertTrue(rateService.defineAllUserRates(1) != null);
        assertTrue(rateService.defineAllUserWinLevelId(1) != null);
    }

}