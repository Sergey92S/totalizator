package com.shmouradko.totalizator;

import com.shmouradko.totalizator.service.impl.EventService;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class EventServiceTest extends TestCase {

    @Test
    public void test() {
        EventService eventService = EventService.getInstance();
        assertTrue(eventService.defineAllEventInfo(1) != null);
        assertTrue(eventService.defineAllEvents(1) != null);
    }

}