package com.shmouradko.totalizator;

import com.shmouradko.totalizator.service.impl.WinLevelService;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class WinLevelServiceTest extends TestCase {

    @Test
    public void test() {
        WinLevelService winLevelService = WinLevelService.getInstance();
        assertTrue(winLevelService.defineAllWinLevelInfo(1) != null);
        assertTrue(winLevelService.defineAllWinLevels(1) != null);
    }

}
