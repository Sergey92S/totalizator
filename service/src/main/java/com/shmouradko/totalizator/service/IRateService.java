package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.viewobject.RateInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IRateService {

    /**
     * find all Person's Win Level Id
     * @param id Person's Id
     * @return Person's Win Level Id
     */
    List<Long> defineAllUserWinLevelId(long id);

    /**
     * find all Person's Rates
     * @param id Person's Id
     * @return Person's Rates
     */
    List<RateInfo> defineAllUserRates(long id);

    /**
     * create new Rate
     * @param personId Person's Id
     * @param winLevelId Win Level's Id
     * @param amount Person's amount
     * @return Person
     */
    Person createRate(long personId, long winLevelId, double amount);

    /**
     * delete Rate
     * @param personId Person's Id
     * @param matchId Match's Id
     */
    void deleteRate(long matchId, long personId);

}
