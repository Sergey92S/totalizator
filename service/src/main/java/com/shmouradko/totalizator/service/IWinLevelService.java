package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.viewobject.WinLevelInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IWinLevelService {
    void createWinLevel(String win, String draw, String defeat, long matchId);

    /**
     * find all Win Levels
     * @param id Match's Id
     * @return Win Levels
     */
    List<WinLevel> defineAllWinLevels(long id);

    /**
     * find all Win Level Information
     * @param id Match's Id
     * @return Win Level Informations
     */
    List<WinLevelInfo> defineAllWinLevelInfo(long id);

    /**
     * find Win Level Information
     * @param id Win Level's Id
     * @return Win Level Information
     */
    WinLevelInfo defineWinLevelInfo(long id);

}
