package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.Team;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface ITeamService {

    /**
     * find Team by Id
     * @param id Team's Id
     * @return Team
     */
    Team defineTeam(long id);

    /**
     * find all Teams
     * @param id Competition's Id
     * @return Teams
     */
    List<Team> defineAllTeams(long id);

    /**
     * find Team Id by it's name
     * @param name Team's Id
     * @return Team Id
     */
    long findTeamIdByName(String name);

}
