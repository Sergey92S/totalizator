package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.SportType;

import java.util.List;

/**
 * Created by test on 30.01.2017.
 */
public interface ISportTypeService {

    /**
     * get all sport types
     * @return List of sport types
     */
    List<SportType> getAllSportTypes();

}
