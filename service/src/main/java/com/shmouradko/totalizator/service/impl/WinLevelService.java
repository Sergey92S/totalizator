package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.WinLevelDao;
import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.IWinLevelService;
import com.shmouradko.totalizator.viewobject.WinLevelInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 10.01.2017.
 */
public class WinLevelService implements IWinLevelService {
    private WinLevelDao dao;
    private EventService eventService = EventService.getInstance();
    public static WinLevelService instance;

    public WinLevelService() {
        dao = WinLevelDao.getInstance();
    }

    public static WinLevelService getInstance() {

        if (instance == null) {
            instance = new WinLevelService();
        }
        return instance;
    }

    @Override
    public void createWinLevel(String win, String draw, String defeat, long matchId){
        try {
            WinLevel winLevel = new WinLevel();
            winLevel.setBet(WinLevel.BetType.WIN);
            winLevel.setCoefficient(Float.parseFloat(win));
            winLevel.setMatchId(matchId);
            dao.saveOrUpdate(winLevel);

            winLevel = new WinLevel();
            winLevel.setBet(WinLevel.BetType.DRAW);
            winLevel.setCoefficient(Float.parseFloat(draw));
            winLevel.setMatchId(matchId);
            dao.saveOrUpdate(winLevel);

            winLevel = new WinLevel();
            winLevel.setBet(WinLevel.BetType.DEFEAT);
            winLevel.setCoefficient(Float.parseFloat(defeat));
            winLevel.setMatchId(matchId);
            dao.saveOrUpdate(winLevel);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<WinLevel> defineAllWinLevels(long id){
        try {
            return dao.getAllWinLevels(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<WinLevelInfo> defineAllWinLevelInfo(long id){
        List<WinLevel> winLevelList = defineAllWinLevels(id);
        List<WinLevelInfo> winLevelInfoList = new ArrayList<>();
        WinLevelInfo winLevelInfo;
        for (WinLevel winLevel: winLevelList) {
            winLevelInfo = new WinLevelInfo();
            winLevelInfo.setCoefficient(winLevel.getCoefficient());
            winLevelInfo.setBet(winLevel.getBet());
            winLevelInfoList.add(winLevelInfo);
        }
        return winLevelInfoList;
    }

    @Override
    public WinLevelInfo defineWinLevelInfo(long id){
        try {
            WinLevelInfo winLevelInfo = dao.getWinLevelInfo(id);
            winLevelInfo.setEventInfoList(eventService.defineAllEventInfo(winLevelInfo.getMatchId()));
            return winLevelInfo;
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }
}
