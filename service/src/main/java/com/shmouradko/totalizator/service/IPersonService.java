package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.PersonService;

import java.util.List;

/**
 * Created by test on 24.01.2017.
 */
public interface IPersonService {

    /**
     * check if such login is in the DB
     * @param login Person's login
     * @return result of checking
     */
    boolean isLoginExist(String login);

    /**
     * create new Person
     * @param login Person's login
     * @param password Person's password
     * @param name Person's name
     * @param surname Person's surname
     * @param age Person's age
     * @param country Person's country
     * @param secretQuestion Person's secret question
     * @param secretAnswer Person's secret answer
     * @param emailArray list of emails
     * @return Person
     */
    Person createPerson(String login, String password, String name, String surname, String age, String country, String secretQuestion, String secretAnswer, String... emailArray);

    /**
     * find Person by login and password
     * @param login Person's login
     * @param password Person's password
     * @return Person
     */
    Person findPersonByCredentials(String login, String password);

    /**
     * update Person's balance
     * @param id Person's Id
     * @param money amount
     * @param operation increase or decrease amount
     * @return Person
     */
    Person updateBalance(long id, double money, PersonService.Operation operation);

    /**
     * find all Person's login
     * @return Person's logins
     */
    List<String> findAllPersonLogin();

    /**
     * update all Persons who won the bet
     * @param matchId Match's Id
     * @param result Match's result
     */
    void updateAllPersons(long matchId, String result);
}
