package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.viewobject.MatchInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IMatchService {

    /**
     * checking if Match with such name is exist at DB
     * @param match Match's Name
     * @return result of checking
     */
    boolean isMatchExist(String match);

    /**
     * create new Match
     * @param competitionId Competition's Id
     * @param name Match's Name
     * @param date defines date
     * @param hour defines hour
     * @param minute defines minute
     * @param home home team
     * @param away away team
     * @param win win bet
     * @param draw draw bet
     * @param defeat defeat bet
     */
    void createMatch(long competitionId, String name, String date, String hour, String minute, String home, String away, String win, String draw, String defeat);

    /**
     * find all Competition Matches
     * @param id Competition's Id
     * @return Matches
     */
    List<Match> defineAllMatches(long id);

    /**
     * find all Competition Match Information
     * @param id Competition's Name
     * @return Match Informations
     */
    List<MatchInfo> defineAllMatchInfo(long id);

    /**
     * find Match Information by id
     * @param id Match's Id
     * @return result of checking
     */
    MatchInfo defineMatchInfo(long id);

    /**
     * update result of empty Match
     * @param id Match's Id
     * @param result Match's result
     */
    void updateResult(long id, String result);

}
