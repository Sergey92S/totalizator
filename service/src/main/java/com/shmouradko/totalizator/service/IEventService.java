package com.shmouradko.totalizator.service;

import com.shmouradko.totalizator.entity.Event;
import com.shmouradko.totalizator.viewobject.EventInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IEventService {

    /**
     * create two Events of Match
     * @param home defines what Event is play at home
     * @param away defines what Event is play away
     * @param matchId Match's Id
     */
    void createEvent(String home, String away, long matchId);

    /**
     * find all Events by Match
     * @param id Match's Id
     * @return List of events
     */
    List<Event> defineAllEvents(long id);

    /**
     * find all Event Information by Match
     * @param id Match's Id
     * @return List of Event Informations
     */
    List<EventInfo> defineAllEventInfo(long id);

}
