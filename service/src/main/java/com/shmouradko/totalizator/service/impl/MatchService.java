package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.MatchDao;
import com.shmouradko.totalizator.entity.Event;
import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.Team;
import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.IMatchService;
import com.shmouradko.totalizator.viewobject.MatchInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Revotech on 09.01.2017.
 */
public class MatchService implements IMatchService {
    private MatchDao dao;
    private EventService eventService = EventService.getInstance();
    private WinLevelService winLevelService = WinLevelService.getInstance();
    private PersonService personService = PersonService.getInstance();
    public static MatchService instance;

    public MatchService() {
        dao = MatchDao.getInstance();
    }

    public static MatchService getInstance() {
        if (instance == null) {
            instance = new MatchService();
        }
        return instance;
    }

    @Override
    public List<Match> defineAllMatches(long id){
        try {
            return dao.getAllMatches(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<MatchInfo> defineAllMatchInfo(long id) {
        List<MatchInfo> matchInfoList = new ArrayList<>();
        List<Match> matches;
        matches = defineAllMatches(id);
        MatchInfo matchInfo;
        for(Match match: matches) {
            matchInfo = new MatchInfo();
            matchInfo.setId(match.getId());
            matchInfo.setName(match.getName());
            matchInfo.setTime(match.getTime());
            matchInfo.setResult(match.getResult());
            matchInfo.setEventInfoList(eventService.defineAllEventInfo(match.getId()));
            matchInfo.setWinLevelList(winLevelService.defineAllWinLevels(match.getId()));
            matchInfoList.add(matchInfo);
        }
        return matchInfoList;
    }

    @Override
    public MatchInfo defineMatchInfo(long id){
        try {
            Match match = dao.find(id);
            MatchInfo matchInfo = new MatchInfo();
            matchInfo.setId(match.getId());
            matchInfo.setName(match.getName());
            matchInfo.setTime(match.getTime());
            matchInfo.setResult(match.getResult());
            matchInfo.setEventInfoList(eventService.defineAllEventInfo(id));
            matchInfo.setWinLevelList(winLevelService.defineAllWinLevels(id));
        return matchInfo;
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isMatchExist(String match){
        try {
            return dao.isMatchExist(match);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateResult(long id, String result){
        try {
            personService.updateAllPersons(id, result);
            Match match = dao.find(id);
            match.setResult(Match.ResultType.valueOf(result));
            dao.saveOrUpdate(match);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createMatch(long competitionId, String name, String date, String hour, String minute, String home, String away, String win, String draw, String defeat){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
            Match match = new Match();
            match.setName(name);
            match.setResult(Match.ResultType.EMPTY);
            match.setTime(sdf.parse(date+" "+hour+":"+minute));
            match.setCompetitionId(competitionId);
            dao.saveOrUpdate(match);
            long matchId = dao.findMatchIdByName(name);
            eventService.createEvent(home, away, matchId);
            winLevelService.createWinLevel(win, draw, defeat, matchId);
        } catch (DaoException | ParseException e){
            throw new RuntimeException(e);
        }
    }

}
