package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.EventDao;
import com.shmouradko.totalizator.entity.Event;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.IEventService;
import com.shmouradko.totalizator.viewobject.EventInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 09.01.2017.
 */
public class EventService implements IEventService {
    private EventDao dao;
    private TeamService teamService = TeamService.getInstance();
    public static EventService instance;

    public EventService() {
        dao = EventDao.getInstance();
    }

    public static EventService getInstance() {
        if (instance == null) {
            instance = new EventService();
        }
        return instance;
    }

    @Override
    public void createEvent(String home, String away, long matchId){
        try {
            Event event = new Event();
            event.setHome(true);
            event.setMatchId(matchId);
            event.setTeamId(teamService.findTeamIdByName(home));
            dao.saveOrUpdate(event);
            event = new Event();
            event.setHome(false);
            event.setMatchId(matchId);
            event.setTeamId(teamService.findTeamIdByName(away));
            dao.saveOrUpdate(event);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Event> defineAllEvents(long id){
        try {
            return dao.getAllEvents(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<EventInfo> defineAllEventInfo(long id){
        List<Event> eventList = defineAllEvents(id);
        List<EventInfo> eventInfoList = new ArrayList<>();
        EventInfo eventInfo;
        for (Event event: eventList) {
            eventInfo = new EventInfo();
            eventInfo.setName(teamService.defineTeam(event.getId()).getName());
            eventInfo.setHome(event.isHome());
            eventInfoList.add(eventInfo);
        }
        return eventInfoList;
    }
}
