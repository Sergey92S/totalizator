package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.RateDao;
import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.entity.Rate;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.IRateService;
import com.shmouradko.totalizator.viewobject.EventInfo;
import com.shmouradko.totalizator.viewobject.RateInfo;

import java.util.List;

/**
 * Created by Revotech on 11.01.2017.
 */
public class RateService implements IRateService {
    private RateDao dao;
    public static RateService instance;
    private EventService eventService = EventService.getInstance();
    private PersonService personService = PersonService.getInstance();

    public RateService() {
        dao = RateDao.getInstance();
    }

    public static RateService getInstance() {
        if (instance == null) {
            instance = new RateService();
        }
        return instance;
    }

    @Override
    public List<Long> defineAllUserWinLevelId(long id){
        try {
            return dao.getAllUserWinLevelId(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<RateInfo> defineAllUserRates(long id){
        try {
            List<RateInfo> rateInfoList = dao.getAllUserRates(id);
            for (RateInfo rateInfo : rateInfoList) {
                List<EventInfo> eventInfoList = eventService.defineAllEventInfo(rateInfo.getId());
                rateInfo.setEventInfoList(eventInfoList);
            }
            return rateInfoList;
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteRate(long matchId, long personId){
        try {
            dao.deleteRate(matchId, personId);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person createRate(long personId, long winLevelId, double amount){
        try {
            Rate rate = new Rate();
            rate.setAmount(amount);
            rate.setPersonId(personId);
            rate.setWinLevelId(winLevelId);
            dao.saveOrUpdate(rate);
            return personService.updateBalance(personId, amount, PersonService.Operation.DECREASE);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

}
