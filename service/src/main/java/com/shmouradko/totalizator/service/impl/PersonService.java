package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.PersonDao;
import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.IPersonService;

import java.util.List;
import java.util.Map;

/**
 * Created by test on 19.01.2017.
 */
public class PersonService implements IPersonService {
    private PersonDao dao;
    public static PersonService instance;
    private static final double DEFAULT_BALANCE = 0.0;
    private static final int USER_ROLE = 1;
    private static final String EMAIL_DELIMETER = " ";

    public enum Operation {
        INCREASE{
            @Override
            double runOperation(double balance, double amount) {
                return balance + amount;
            }
        },
        DECREASE{
            @Override
            double runOperation(double balance, double amount) {
                return balance - amount;
            }
        };

        abstract double runOperation(double balance, double amount);

    }

    public PersonService() {
        dao = PersonDao.getInstance();
    }

    public static PersonService getInstance() {
        if (instance == null) {
            instance = new PersonService();
        }
        return instance;
    }

    @Override
    public List<String> findAllPersonLogin(){
        try {
            return dao.findAllPersonLogin();
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isLoginExist(String login){
        try {
            return dao.isLoginExist(login);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person createPerson(String login, String password, String name, String surname, String age, String country, String secretQuestion, String secretAnswer, String... emailArray){
        try {
            Person person = new Person();
            person.setLogin(login);
            person.setPassword(password);
            person.setName(name);
            person.setSurname(surname);
            person.setAge(Integer.parseInt(age));
            person.setCountry(country);
            person.setSecretQuestion(secretQuestion);
            person.setSecretAnswer(secretAnswer);
            StringBuilder personEmail = new StringBuilder();
            for(String email: emailArray){
                if(email!=null){
                    personEmail.append(email+EMAIL_DELIMETER);
                }
            }
            person.setEmail(personEmail.toString());
            person.setBalance(DEFAULT_BALANCE);
            person.setRole(USER_ROLE);
            dao.saveOrUpdate(person);
            return dao.findPersonByCredentials(login, password);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person findPersonByCredentials(String login, String password){
        try {
            return dao.findPersonByCredentials(login, password);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateAllPersons(long matchId, String result){
        try {
            Map<Long, Float> personData = dao.findAllUpdatePersons(matchId, result);
            for (Map.Entry<Long, Float> entry : personData.entrySet()) {
                updateBalance(entry.getKey(), entry.getValue(), Operation.INCREASE);
            }
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Person updateBalance(long id, double money, Operation operation){
        try {
            Person person = dao.find(id);
            person.setBalance(operation.runOperation(person.getBalance(), money));
            dao.saveOrUpdate(person);
            return person;
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

}
