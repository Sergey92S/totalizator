package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.SportTypeDao;
import com.shmouradko.totalizator.entity.SportType;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.ISportTypeService;

import java.util.List;

/**
 * Created by test on 30.01.2017.
 */
public class SportTypeService implements ISportTypeService {
    private SportTypeDao dao;
    public static SportTypeService instance;

    public SportTypeService() {
        dao = SportTypeDao.getInstance();
    }

    public static SportTypeService getInstance() {
        if (instance == null) {
            instance = new SportTypeService();
        }
        return instance;
    }

    @Override
    public List<SportType> getAllSportTypes() {
        try {
            return dao.getAllSportTypes();
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }
}
