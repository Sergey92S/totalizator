package com.shmouradko.totalizator.service.impl;

import com.shmouradko.totalizator.dao.impl.TeamDao;
import com.shmouradko.totalizator.entity.Team;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.service.ITeamService;

import java.util.List;

/**
 * Created by Revotech on 10.01.2017.
 */
public class TeamService implements ITeamService {
    private TeamDao dao;
    public static TeamService instance;

    public TeamService() {
        dao = TeamDao.getInstance();
    }

    public static TeamService getInstance() {
        if (instance == null) {
            instance = new TeamService();
        }
        return instance;
    }

    @Override
    public Team defineTeam(long id){
        try {
            return dao.find(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Team> defineAllTeams(long id){
        try {
            return dao.getAllTeams(id);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public long findTeamIdByName(String name){
        try {
            return dao.findTeamIdByName(name);
        } catch (DaoException e){
            throw new RuntimeException(e);
        }
    }

}
