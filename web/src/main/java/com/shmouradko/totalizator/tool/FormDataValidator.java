package com.shmouradko.totalizator.tool;

import java.util.regex.Pattern;

/**
 * Created by test on 23.01.2017.
 */
public class FormDataValidator {

    /**
     * Required: Beginning from an uppercase or a lowercase letter
     * and border from 4 to 20 symbols
     * Example: sergey_shmouradko.
     */
    public static final Pattern loginPattern = Pattern.compile("^[a-zA-Z][\\w]{4,20}$");

    //(?=^.{6,20}$)(?=.*[\d])(?=.*[a-z])(?=.*[A-Z]).*$

    /**
     * Required: At least one lowercase and one uppercase letters and one digital
     * also password has a border from 6 to 20 symbols
     * Example: Sergey92
     */
    public static final Pattern passwordPattern = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[\\d]).{6,20}$");

    //^[A-Z][a-z]{1,20}$

    /**
     * Required: Only latin or only russian letters from 1 to 15 symbols and begin from uppercase
     * Example: Sergey
     */
    public static final Pattern namePattern = Pattern.compile("^[A-Z][a-z]{1,15}$");

    /**
     * Required: Only latin or only russian letters from 1 to 15 symbols and begin from uppercase
     * Example: Shmouradko
     */
    public static final Pattern surnamePattern = Pattern.compile("^[A-Z][a-z]{1,15}$");

    /**
     * Required: age between 7 and 120 years
     * Example: 24
     */
    public static final Pattern agePattern = Pattern.compile("^([789]|\\d\\d|1[01]\\d|120)$");

    /**
     * Required: One @ and . symbols
     * Example: sshmouradko@outlook.com
     */
    public static final Pattern emailPattern = Pattern
            .compile("^[a-z0-9]+@[a-z0-9]+\\.[a-z]{2,}$");

    /**
     * Required: One digital
     * Example: 1
     */
    public static final Pattern competitionIdPattern = Pattern.compile("\\d");

}
