package com.shmouradko.totalizator.creator;

import com.shmouradko.totalizator.command.*;

/**
 * Created by test on 23.11.2016.
 */
public enum CommandCreator {
    COMPETITION {
        {
            this.command = new CompetitionCommand();
        }
    },
    LOGON {
        {
            this.command = new LoginCommand();
        }
    },
    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },
    REGISTER{
        {
            this.command = new RegisterCommand();
        }
    },
    CANCEL {
        {
            this.command = new CancelCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    MATCH {
        {
            this.command = new MatchCommand();
        }
    },
    RATE {
        {
            this.command = new RateCommand();
        }
    },
    UPDATEBALANCE {
        {
            this.command = new UpdateBalanceCommand();
        }
    },
    BET {
        {
            this.command = new BetCommand();
        }
    },
    CREATE {
        {
            this.command = new CreateCommand();
        }
    },
    UPDATE {
        {
            this.command = new UpdateMatchCommand();
        }
    },
    DELETE {
        {
            this.command = new DeleteRateCommand();
        }
    };
    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
