package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.tool.PageManager;

/**
 * Created by test on 23.11.2016.
 */
public abstract class ActionCommand implements ICommand {
    protected static PageManager pageManager;

    // REQUEST ATTRIBUTES
    protected static final String TIME_ATTRIBUTE = "time";
    protected static final String ALL_PERSON_LOGIN_ATTRIBUTE = "loginList";

    protected static final String MATCH_NAME_ATTRIBUTE = "name";
    protected static final String MATCH_DATE_ATTRIBUTE = "date";
    protected static final String MATH_HOUR_ATTRIBUTE = "hour";
    protected static final String MATCH_MINUTE_ATTRIBUTE = "minute";
    protected static final String HOME_TEAM_ATTRIBUTE = "home";
    protected static final String AWAY_TEAM_ATTRIBUTE = "away";
    protected static final String WIN_ATTRIBUTE = "win";
    protected static final String DRAW_ATTRIBUTE = "draw";
    protected static final String DEFEAT_ATTRIBUTE = "defeat";

    protected static final String TEAM_ATTRIBUTE = "teams";
    protected static final String AMOUNT_ATTRIBUTE = "amount";
    protected static final String MONEY_ATTRIBUTE = "money";
    protected static final String COMPETITION_ID_ATTRIBUTE = "competitionID";
    protected static final String MATCH_ID_ATTRIBUTE = "matchId";
    protected static final String RATE_ATTRIBUTE = "rateResult";
    protected static final String WIN_LEVEL_ATTRIBUTE = "winLevel";
    protected static final String WIN_LEVEL_ID_ATTRIBUTE = "winLevelUser";
    protected static final String TOTAL_COUNT_ATTRIBUTE = "totalCount";
    protected static final String RESULT_ATTRIBUTE = "result";
    protected static final String MATCH_RESULT_ATTRIBUTE = "matchResult";
    protected static final String PERSON_ATTRIBUTE = "person";
    protected static final String PERSON_TYPE_ATTRIBUTE = "personType";
    protected static final String PERSON_LOGIN_ATTRIBUTE = "login";
    protected static final String PERSON_PASSWORD_ATTRIBUTE = "password";
    protected static final String PERSON_NAME_ATTRIBUTE = "name";
    protected static final String PERSON_SURNAME_ATTRIBUTE = "surname";
    protected static final String PERSON_AGE_ATTRIBUTE = "age";
    protected static final String PERSON_COUNTRY_ATTRIBUTE = "country";
    protected static final String PERSON_SECRET_QUESTION_ATTRIBUTE = "question";
    protected static final String PERSON_SECRET_ANSWER_ATTRIBUTE = "answer";
    protected static final String PERSON_EMAIL_1_ATTRIBUTE = "email_1";
    protected static final String PERSON_EMAIL_2_ATTRIBUTE = "email_2";
    protected static final String PERSON_EMAIL_3_ATTRIBUTE = "email_3";

    // ATTRIBUTE DEFINITION
    protected static final String ERROR_EMPTY_NAME_MESSAGE = "There is an empty Match's name!!!";
    protected static final String ERROR_EXIST_NAME_MESSAGE = "This name has already exist!!!";
    protected static final String ERROR_SAME_TEAMS_MESSAGE = "You choose one team twice!!!";

    protected static final String ERROR_LOGIN_MESSAGE = "There is no such person!!!";
    protected static final String ERROR_REGISTRATION_LOGIN_MESSAGE = "This login is already exist!!!";
    protected static final String ERROR_REGISTRATION_MESSAGE = "Wrong validation!!!";

    protected static final String ERROR_AMOUNT_MESSAGE = "Wrong validation!!!";

    // REQUEST ERRORS
    protected static final String ERROR_ATTRIBUTE = "errorMessage";

    protected ActionCommand(){
        pageManager = PageManager.getInstance();
    }

}
