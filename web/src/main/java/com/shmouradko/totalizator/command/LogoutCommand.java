package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.service.impl.CompetitionService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 08.01.2017.
 */
public class LogoutCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        CompetitionService competitionService = CompetitionService.getInstance();
        request.getSession().setAttribute(PERSON_TYPE_ATTRIBUTE,
                LoginCommand.ClientType.GUEST);
        request.getSession().setAttribute(RESULT_ATTRIBUTE, competitionService.getAllGames());
        request.getSession().setAttribute(TOTAL_COUNT_ATTRIBUTE, competitionService.getAllGames().size());
        page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        return page;
    }
}
