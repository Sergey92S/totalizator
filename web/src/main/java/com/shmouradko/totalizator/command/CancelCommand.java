package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Revotech on 30.01.2017.
 */
public class CancelCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE) == null){
            page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        } else {
            page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH);
        }
        return page;
    }
}
