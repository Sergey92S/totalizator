package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.PersonService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 19.01.2017.
 */
public class UpdateBalanceCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        PersonService personService = new PersonService();
        request.getSession().setAttribute(PERSON_ATTRIBUTE, personService.updateBalance(((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getId(), Double.parseDouble(request.getParameter(MONEY_ATTRIBUTE)), PersonService.Operation.INCREASE));
        page = pageManager.getProperty(PageManager.PATH_PAGE_RATE);
        return page;
    }
}
