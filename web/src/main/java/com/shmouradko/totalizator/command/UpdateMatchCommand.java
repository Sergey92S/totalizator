package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.service.impl.MatchService;
import com.shmouradko.totalizator.service.impl.TeamService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 29.01.2017.
 */
public class UpdateMatchCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        MatchService matchService = MatchService.getInstance();
        TeamService teamService = TeamService.getInstance();
        request.getSession().setAttribute(TEAM_ATTRIBUTE, teamService.defineAllTeams(Long.parseLong(request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE).toString())));
        matchService.updateResult(Long.parseLong(request.getParameter(MATCH_ID_ATTRIBUTE)), request.getParameter(RESULT_ATTRIBUTE));
        request.getSession().setAttribute(MATCH_RESULT_ATTRIBUTE, matchService.defineAllMatchInfo(Long.parseLong(request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE).toString())));
        page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        return page;
    }
}
