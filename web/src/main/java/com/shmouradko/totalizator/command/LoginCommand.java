package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.PersonService;
import com.shmouradko.totalizator.service.impl.RateService;
import com.shmouradko.totalizator.service.impl.TeamService;
import com.shmouradko.totalizator.tool.PageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.shmouradko.totalizator.tool.FormDataValidator.loginPattern;
import static com.shmouradko.totalizator.tool.FormDataValidator.passwordPattern;

/**
 * Created by Revotech on 05.01.2017.
 */
public class LoginCommand extends ActionCommand implements ICommand{

    public enum ClientType {
        GUEST, USER, ADMIN
    }

    private static Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        PersonService personService = PersonService.getInstance();
        RateService rateService = RateService.getInstance();
        TeamService teamService = TeamService.getInstance();
        String login;
        String password;
        if(request.getParameter(PERSON_LOGIN_ATTRIBUTE) != null && request.getParameter(PERSON_PASSWORD_ATTRIBUTE) != null){
            login = request.getParameter(PERSON_LOGIN_ATTRIBUTE);
            password = request.getParameter(PERSON_PASSWORD_ATTRIBUTE);
        } else if(((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getLogin() != null && ((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getPassword() != null){
            login = ((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getLogin();
            password = ((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getPassword();
        } else {
            return PageManager.PATH_PAGE_INDEX;
        }
        List<String> loginList = personService.findAllPersonLogin();
        request.getSession().setAttribute(ALL_PERSON_LOGIN_ATTRIBUTE, loginList);

        if(validation(login, password, loginList)){
            Person person = personService.findPersonByCredentials(login, password);
            switch (person.getRole()) {
                case 1:
                    request.getSession().setAttribute(PERSON_ATTRIBUTE, person);
                    request.getSession().setAttribute(PERSON_TYPE_ATTRIBUTE,
                        ClientType.USER);
                    request.getSession().setAttribute(PERSON_LOGIN_ATTRIBUTE,
                            person.getLogin());
                    request.getSession().setAttribute(PERSON_PASSWORD_ATTRIBUTE,
                            person.getPassword());
                    if (request.getParameter(COMPETITION_ID_ATTRIBUTE) != null){
                        request.getSession().setAttribute(WIN_LEVEL_ID_ATTRIBUTE, rateService.defineAllUserWinLevelId(((Person)(request.getSession().getAttribute(PERSON_ATTRIBUTE))).getId()));
                        request.getSession().setAttribute(TIME_ATTRIBUTE, new Date(System.currentTimeMillis()));
                        page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_USER);
                    } else {
                        page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION_USER);
                    }
                    break;
                case 2:
                    request.getSession().setAttribute(PERSON_ATTRIBUTE, person);
                    request.getSession().setAttribute(PERSON_TYPE_ATTRIBUTE,
                            ClientType.ADMIN);
                    request.setAttribute(PERSON_LOGIN_ATTRIBUTE,
                            person.getLogin());
                    request.setAttribute(PERSON_PASSWORD_ATTRIBUTE,
                            person.getPassword());
                    if (request.getParameter(COMPETITION_ID_ATTRIBUTE) != null){
                        request.getSession().setAttribute(TEAM_ATTRIBUTE, teamService.defineAllTeams(Long.parseLong(request.getParameter(COMPETITION_ID_ATTRIBUTE))));
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(System.currentTimeMillis());
                        calendar.add(Calendar.DATE, 1);
                        request.getSession().setAttribute(TIME_ATTRIBUTE, new Date(calendar.getTimeInMillis()));
                        page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
                    } else {
                        page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION_ADMIN);
                    }
                    break;
                default:
                    LOGGER.log(Level.WARN, ERROR_LOGIN_MESSAGE);
                    request.getSession().setAttribute(PERSON_ATTRIBUTE,
                            ClientType.GUEST);
                    request.getSession().setAttribute(ERROR_ATTRIBUTE,
                            ERROR_LOGIN_MESSAGE);
                    page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
            }
        }else {
            LOGGER.log(Level.WARN, ERROR_LOGIN_MESSAGE);
            request.getSession().setAttribute(PERSON_TYPE_ATTRIBUTE,
                    ClientType.GUEST);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_LOGIN_MESSAGE);
            page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        }
        LOGGER.log(Level.DEBUG, page);
        return page;
    }

    private boolean validation(String login, String password, List<String> loginList){

        if(!loginList.contains(login)){
            return false;
        }

        if (!loginPattern.matcher(login).matches()) {
            return false;
        }

        if (!passwordPattern.matcher(password).matches()) {
            return false;
        }

        return true;
    }

}
