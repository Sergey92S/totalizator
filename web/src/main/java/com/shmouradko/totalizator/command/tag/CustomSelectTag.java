package com.shmouradko.totalizator.command.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by test on 22.01.2017.
 */
public class CustomSelectTag extends TagSupport {
    private static final int DEFAULT_NUMBERS_OF_DAYS = 7;
    private Calendar calendar = Calendar.getInstance();

    @Override
    public int doEndTag() throws JspException {
        try {
            pageContext.getOut().write("</select>");
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_PAGE;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.DATE, DEFAULT_NUMBERS_OF_DAYS);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            out.write("<select name=\"date\">");
            for(int i = 0; i < DEFAULT_NUMBERS_OF_DAYS; i++) {
                pageContext.getOut().write("<option>"+sdf.format(calendar.getTime())+"</option>");
                calendar.add(Calendar.DATE, 1);
            }
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return SKIP_BODY;
    }

}