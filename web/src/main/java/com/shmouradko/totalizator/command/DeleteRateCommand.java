package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.RateService;
import com.shmouradko.totalizator.service.impl.WinLevelService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by test on 29.01.2017.
 */
public class DeleteRateCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        RateService rateService = RateService.getInstance();
        WinLevelService winLevelService = WinLevelService.getInstance();
        rateService.deleteRate(Long.parseLong(request.getParameter(MATCH_ID_ATTRIBUTE)), ((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getId());
        request.getSession().setAttribute(WIN_LEVEL_ATTRIBUTE, winLevelService.defineWinLevelInfo(Long.parseLong(request.getSession().getAttribute(WIN_LEVEL_ID_ATTRIBUTE).toString())));
        request.getSession().setAttribute(RATE_ATTRIBUTE, rateService.defineAllUserRates(((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getId()));
        request.getSession().setAttribute(TIME_ATTRIBUTE, new Date(System.currentTimeMillis()));
        page = pageManager.getProperty(PageManager.PATH_PAGE_RATE);
        return page;
    }
}
