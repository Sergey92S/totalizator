package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.RateService;
import com.shmouradko.totalizator.tool.PageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Revotech on 12.01.2017.
 */
public class BetCommand extends ActionCommand {
    private static Logger LOGGER = LogManager.getLogger(BetCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        RateService rateService = new RateService();
        if(validation(request.getParameter(AMOUNT_ATTRIBUTE), ((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getBalance())){
            request.getSession().setAttribute(PERSON_ATTRIBUTE, rateService.createRate(((Person)request.getSession().getAttribute(PERSON_ATTRIBUTE)).getId(), Long.parseLong(request.getParameter(WIN_LEVEL_ID_ATTRIBUTE)), Double.parseDouble(request.getParameter(AMOUNT_ATTRIBUTE))));
            request.getSession().setAttribute(WIN_LEVEL_ID_ATTRIBUTE, rateService.defineAllUserWinLevelId(((Person)(request.getSession().getAttribute(PERSON_ATTRIBUTE))).getId()));
            page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_USER);
        } else {
            LOGGER.log(Level.WARN, ERROR_AMOUNT_MESSAGE);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_AMOUNT_MESSAGE);
            page = pageManager.getProperty(PageManager.PATH_PAGE_RATE);
        }

        return page;
    }

    private boolean validation(String amount, double balance){

        if(amount == null || amount.isEmpty()){
            return false;
        }

        if((Double.parseDouble(amount) > balance) || (Double.parseDouble(amount) < 0)){
            return false;
        }

        return true;

    }

}
