package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.service.impl.MatchService;
import com.shmouradko.totalizator.service.impl.TeamService;
import com.shmouradko.totalizator.tool.PageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 28.01.2017.
 */
public class CreateCommand extends ActionCommand {
    private static Logger LOGGER = LogManager.getLogger(CompetitionCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        MatchService matchService = new MatchService();
        TeamService teamService = new TeamService();
        if(!validation(request.getParameter(MATCH_NAME_ATTRIBUTE))){
            LOGGER.log(Level.WARN, ERROR_EMPTY_NAME_MESSAGE);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_EMPTY_NAME_MESSAGE);
            return pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        }
        if(request.getParameter(HOME_TEAM_ATTRIBUTE).equals(request.getParameter(AWAY_TEAM_ATTRIBUTE))){
            LOGGER.log(Level.WARN, ERROR_SAME_TEAMS_MESSAGE);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_SAME_TEAMS_MESSAGE);
            return pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        }
        if(matchService.isMatchExist(request.getParameter(MATCH_NAME_ATTRIBUTE))){
            LOGGER.log(Level.WARN, ERROR_EXIST_NAME_MESSAGE);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_EXIST_NAME_MESSAGE);
            return pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        }
        request.getSession().setAttribute(TEAM_ATTRIBUTE, teamService.defineAllTeams(Long.parseLong(request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE).toString())));
        matchService.createMatch(Long.parseLong(request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE).toString()), request.getParameter(MATCH_NAME_ATTRIBUTE), request.getParameter(MATCH_DATE_ATTRIBUTE), request.getParameter(MATH_HOUR_ATTRIBUTE), request.getParameter(MATCH_MINUTE_ATTRIBUTE), request.getParameter(HOME_TEAM_ATTRIBUTE), request.getParameter(AWAY_TEAM_ATTRIBUTE), request.getParameter(WIN_ATTRIBUTE), request.getParameter(DRAW_ATTRIBUTE), request.getParameter(DEFEAT_ATTRIBUTE));
        request.getSession().setAttribute(MATCH_RESULT_ATTRIBUTE, matchService.defineAllMatchInfo(Long.parseLong(request.getSession().getAttribute(COMPETITION_ID_ATTRIBUTE).toString())));
        page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        return page;
    }

    private boolean validation(String name){

        if(name == null || name.isEmpty()){
            return false;
        }

        return true;

    }
}
