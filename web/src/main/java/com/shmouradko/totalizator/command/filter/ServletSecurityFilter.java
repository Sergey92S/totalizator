package com.shmouradko.totalizator.command.filter;

import com.shmouradko.totalizator.command.LoginCommand;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by test on 24.01.2017.
 */
@WebFilter(urlPatterns = { "/FrontController" }, servletNames = { "FrontController" })
public class ServletSecurityFilter implements Filter {
    private static final String PERSON_TYPE_ATTRIBUTE = "personType";
    private static final String ERROR_ATTRIBUTE = "errorMessage";
    private static final String DEFAULT_MESSAGE = "";

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        LoginCommand.ClientType type = (LoginCommand.ClientType) session.getAttribute(PERSON_TYPE_ATTRIBUTE);
        req.getSession().setAttribute(ERROR_ATTRIBUTE,
                DEFAULT_MESSAGE);
        if (type == null) {
            type = LoginCommand.ClientType.GUEST;
            session.setAttribute(PERSON_TYPE_ATTRIBUTE, type);
            if (session.isNew()) {
                RequestDispatcher dispatcher = ((HttpServletRequest) request).getSession().getServletContext().getRequestDispatcher(PageManager.PATH_PAGE_INDEX);
                dispatcher.forward(req, resp);
                return;
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {

    }

}