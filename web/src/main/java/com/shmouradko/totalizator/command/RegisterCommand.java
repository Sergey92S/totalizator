package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.PersonService;
import com.shmouradko.totalizator.tool.PageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.shmouradko.totalizator.tool.FormDataValidator.*;
import static com.shmouradko.totalizator.tool.FormDataValidator.agePattern;
import static com.shmouradko.totalizator.tool.FormDataValidator.emailPattern;

/**
 * Created by test on 30.01.2017.
 */
public class RegisterCommand extends ActionCommand {
    private static Logger LOGGER = LogManager.getLogger(RegisterCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        PersonService personService = PersonService.getInstance();
        if(validation(request.getParameter(PERSON_LOGIN_ATTRIBUTE), request.getParameter(PERSON_PASSWORD_ATTRIBUTE), request.getParameter(PERSON_NAME_ATTRIBUTE), request.getParameter(PERSON_SURNAME_ATTRIBUTE), request.getParameter(PERSON_AGE_ATTRIBUTE), request.getParameter(PERSON_SECRET_ANSWER_ATTRIBUTE), request.getParameter(PERSON_EMAIL_1_ATTRIBUTE), request.getParameter(PERSON_EMAIL_2_ATTRIBUTE), request.getParameter(PERSON_EMAIL_3_ATTRIBUTE))){

            if(personService.isLoginExist(request.getParameter(PERSON_LOGIN_ATTRIBUTE))){
                LOGGER.log(Level.WARN, ERROR_REGISTRATION_LOGIN_MESSAGE);
                request.getSession().setAttribute(ERROR_ATTRIBUTE,
                        ERROR_REGISTRATION_LOGIN_MESSAGE);
                return pageManager.getProperty(PageManager.PATH_PAGE_REGISTRATION);
            }

            Person person = personService.createPerson(request.getParameter(PERSON_LOGIN_ATTRIBUTE), request.getParameter(PERSON_PASSWORD_ATTRIBUTE), request.getParameter(PERSON_NAME_ATTRIBUTE), request.getParameter(PERSON_SURNAME_ATTRIBUTE), request.getParameter(PERSON_AGE_ATTRIBUTE), request.getParameter(PERSON_COUNTRY_ATTRIBUTE), request.getParameter(PERSON_SECRET_QUESTION_ATTRIBUTE), request.getParameter(PERSON_SECRET_ANSWER_ATTRIBUTE), request.getParameter(PERSON_EMAIL_1_ATTRIBUTE), request.getParameter(PERSON_EMAIL_2_ATTRIBUTE), request.getParameter(PERSON_EMAIL_3_ATTRIBUTE));
            request.getSession().setAttribute(PERSON_ATTRIBUTE, person);
            request.getSession().setAttribute(PERSON_TYPE_ATTRIBUTE,
                    LoginCommand.ClientType.USER);
            page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        } else {
            LOGGER.log(Level.WARN, ERROR_REGISTRATION_MESSAGE);
            request.getSession().setAttribute(ERROR_ATTRIBUTE,
                    ERROR_REGISTRATION_MESSAGE);
            return pageManager.getProperty(PageManager.PATH_PAGE_REGISTRATION);
        }

        return page;
    }

    private static boolean validation(String login, String password, String name, String surname, String age, String secretAnswer, String... emailArray){
        if (!loginPattern.matcher(login).matches()) {
            return false;
        }

        if (!passwordPattern.matcher(password).matches()) {
            return false;
        }

        if (!namePattern.matcher(name).matches()) {
            return false;
        }

        if (!surnamePattern.matcher(surname).matches()) {
            return false;
        }

        if (!agePattern.matcher(age).matches()) {
            return false;
        }

        if (secretAnswer.isEmpty()) {
            return false;
        }

        for(String email: emailArray) {
            if(email!=null){
                if (!emailPattern.matcher(email).matches()) {
                    return false;
                }
            }

        }

        return true;
    }

}
