package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.service.impl.PersonService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 21.01.2017.
 */
public class RegistrationCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        PersonService personService = PersonService.getInstance();
        request.getSession().setAttribute(ALL_PERSON_LOGIN_ATTRIBUTE, personService.findAllPersonLogin());
        page = pageManager.getProperty(PageManager.PATH_PAGE_REGISTRATION);
        return page;
    }

}
