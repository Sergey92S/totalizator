package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.service.impl.CompetitionService;
import com.shmouradko.totalizator.tool.PageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by test on 23.11.2016.
 */
public class CompetitionCommand extends ActionCommand implements ICommand {
    private static Logger LOGGER = LogManager.getLogger(CompetitionCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        CompetitionService competitionService = CompetitionService.getInstance();
        request.getSession().setAttribute(RESULT_ATTRIBUTE, competitionService.getAllGames());
        request.getSession().setAttribute(TOTAL_COUNT_ATTRIBUTE, competitionService.getAllGames().size());
        request.getSession().setAttribute(COMPETITION_ID_ATTRIBUTE, null);

        page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        if(LoginCommand.ClientType.USER.equals(request.getSession().getAttribute(PERSON_TYPE_ATTRIBUTE))){
            page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION_USER);
        }
        if(LoginCommand.ClientType.ADMIN.equals(request.getSession().getAttribute(PERSON_TYPE_ATTRIBUTE))){
            page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION_ADMIN);
        }

        LOGGER.log(Level.DEBUG, page);
        return page;
    }

}
