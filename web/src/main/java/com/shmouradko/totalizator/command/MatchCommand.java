package com.shmouradko.totalizator.command;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.service.impl.MatchService;
import com.shmouradko.totalizator.service.impl.RateService;
import com.shmouradko.totalizator.service.impl.TeamService;
import com.shmouradko.totalizator.tool.PageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

import static com.shmouradko.totalizator.tool.FormDataValidator.competitionIdPattern;

/**
 * Created by test on 09.01.2017.
 */
public class MatchCommand extends ActionCommand {
    private String page;

    @Override
    public String execute(HttpServletRequest request) {
        MatchService matchService = MatchService.getInstance();
        RateService rateService = RateService.getInstance();
        TeamService teamService = TeamService.getInstance();
        if(!validation(request.getParameter(COMPETITION_ID_ATTRIBUTE))){
            return PageManager.PATH_PAGE_INDEX;
        }
        request.getSession().setAttribute(MATCH_RESULT_ATTRIBUTE, matchService.defineAllMatchInfo(Long.parseLong(request.getParameter(COMPETITION_ID_ATTRIBUTE))));
        request.getSession().setAttribute(COMPETITION_ID_ATTRIBUTE, request.getParameter(COMPETITION_ID_ATTRIBUTE));
        page = pageManager.getProperty(PageManager.PATH_PAGE_COMPETITION);
        if(LoginCommand.ClientType.GUEST.equals(request.getSession().getAttribute(PERSON_TYPE_ATTRIBUTE))){
            page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH);
        }
        if(LoginCommand.ClientType.USER.equals(request.getSession().getAttribute(PERSON_TYPE_ATTRIBUTE))){
            request.getSession().setAttribute(WIN_LEVEL_ID_ATTRIBUTE, rateService.defineAllUserWinLevelId(((Person)(request.getSession().getAttribute(PERSON_ATTRIBUTE))).getId()));
            request.getSession().setAttribute(TIME_ATTRIBUTE, new Date(System.currentTimeMillis()));
            page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_USER);
        }
        if(LoginCommand.ClientType.ADMIN.equals(request.getSession().getAttribute(PERSON_TYPE_ATTRIBUTE))){
            request.getSession().setAttribute(TEAM_ATTRIBUTE, teamService.defineAllTeams(Long.parseLong(request.getParameter(COMPETITION_ID_ATTRIBUTE))));
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.DATE, 1);
            request.getSession().setAttribute(TIME_ATTRIBUTE, calendar.getTime());
            page = pageManager.getProperty(PageManager.PATH_PAGE_MATCH_ADMIN);
        }
        return page;
    }

    private boolean validation(String competitionId){

        if (!competitionIdPattern.matcher(competitionId).matches()){
            return false;
        }

        return true;

    }
}
