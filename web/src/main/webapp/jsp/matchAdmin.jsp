<%--
  Created by IntelliJ IDEA.
  User: Revotech
  Date: 26.01.2017
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="/WEB-INF/tld/taglib.tld" prefix="ctg" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>

<html lang="${language}">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><fmt:message key="totalizator.h1"/></title>

    <link rel="stylesheet" type="text/css" href="css/styles.css?<?php echo time(); ?>" />

</head>

<body>

<div class="fields">

    <label><fmt:message key="greeting"/> ${person.getName()} ${person.getSurname()}</label>

    <form name="logoutForm" method="POST" action="FrontController">
        <input type="hidden" name="command" value="logout"/>
        <div class="fields-criteria">
            <button name="logout" value="logout"><fmt:message key="logout"/></button>
        </div>
    </form>

</div>

<section id="page">

    <header>
        <hgroup>
            <h1><fmt:message key="totalizator.h1"/></h1>
            <h3><fmt:message key="totalizator.h3"/></h3>
        </hgroup>

        <nav class="bottom">

            <ul>
                <li><a href="?command=match&competitionID=${competitionID}&language=en" ><fmt:message key="totalizator.language.en"/></a></li>
                <li><a href="?command=match&competitionID=${competitionID}&language=ru" }><fmt:message key="totalizator.language.ru"/></a></li>
            </ul>

        </nav>

        <nav class="top">
            <ul>
                <li>
                    <a href="?command=competition"><fmt:message key="match.back"/></a>
                </li>
            </ul>
        </nav>

    </header>

    <section id="articles">
        <div class="line"></div>



        <article id="article1">
            <h2 align="center"><fmt:message key="match.create"/></h2>
            <div class="line"></div>
            <div align="center"><p><font size="5" color="red">${errorMessage}</font></p></div>
            <ul class="history">
                <li>

                <form onsubmit="return checkName()" name="createMatchForm" method="POST" action="/FrontController">
                    <input type="hidden" name="command" value="create"/>
            <table cellspacing="10" align="center">
                <tr>
                    <th><fmt:message key="match.name2"/></th>
                    <th><fmt:message key="match.date"/></th>
                    <th><fmt:message key="match.team"/></th>
                    <th><fmt:message key="match.winlevel"/></th>
                </tr>

                <tr>
                    <td>
                        <input size="10" id="name" name="name" type="text">
                        <label id='err_name' class='error'></label>
                    </td>
                    <td align="center">
                        <ctg:select/>
                        <select name="hour">
                            <option>00</option>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                            <option>04</option>
                            <option>05</option>
                            <option>06</option>
                            <option>07</option>
                            <option>08</option>
                            <option>09</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>18</option>
                            <option>19</option>
                            <option>20</option>
                            <option>21</option>
                            <option>22</option>
                            <option>23</option>
                        </select>
                        <fmt:message key="match.delimeter"/>
                        <select name="minute">
                            <option>00</option>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                            <option>04</option>
                            <option>05</option>
                            <option>06</option>
                            <option>07</option>
                            <option>08</option>
                            <option>09</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>18</option>
                            <option>19</option>
                            <option>20</option>
                            <option>21</option>
                            <option>22</option>
                            <option>23</option>
                            <option>24</option>
                            <option>25</option>
                            <option>26</option>
                            <option>27</option>
                            <option>28</option>
                            <option>29</option>
                            <option>30</option>
                            <option>31</option>
                            <option>32</option>
                            <option>33</option>
                            <option>34</option>
                            <option>35</option>
                            <option>36</option>
                            <option>37</option>
                            <option>38</option>
                            <option>39</option>
                            <option>40</option>
                            <option>41</option>
                            <option>42</option>
                            <option>43</option>
                            <option>44</option>
                            <option>45</option>
                            <option>46</option>
                            <option>47</option>
                            <option>48</option>
                            <option>49</option>
                            <option>50</option>
                            <option>51</option>
                            <option>52</option>
                            <option>53</option>
                            <option>54</option>
                            <option>55</option>
                            <option>56</option>
                            <option>57</option>
                            <option>58</option>
                            <option>59</option>
                        </select>
                    </td>
                    <td align="center">
                        <select id="home" name="home">
                            <c:forEach var="team" items="${teams}" varStatus="loop">
                                <option value="${team.getName()}">
                                    <c:if test="${team.getName() eq 'Бразилия'}">
                                        <fmt:message key="team.brazil"/>
                                     </c:if>
                                    <c:if test="${team.getName() eq 'Германия'}">
                                        <fmt:message key="team.germany"/>
                                    </c:if>
                                    <c:if test="${team.getName() eq 'Испания'}">
                                        <fmt:message key="team.spain"/>
                                    </c:if>
                                    <c:if test="${team.getName() eq 'Аргентина'}">
                                        <fmt:message key="team.argentina"/>
                                    </c:if>
                                </option>
                            </c:forEach>
                        </select>
                        <fmt:message key="match.versus"/>
                        <select id="away" name="away">
                            <c:forEach var="team" items="${teams}" varStatus="loop">
                                <option value="${team.getName()}">
                                    <c:if test="${team.getName() eq 'Бразилия'}">
                                        <fmt:message key="team.brazil"/>
                                    </c:if>
                                    <c:if test="${team.getName() eq 'Германия'}">
                                        <fmt:message key="team.germany"/>
                                    </c:if>
                                    <c:if test="${team.getName() eq 'Испания'}">
                                        <fmt:message key="team.spain"/>
                                    </c:if>
                                    <c:if test="${team.getName() eq 'Аргентина'}">
                                        <fmt:message key="team.argentina"/>
                                    </c:if>
                                </option>
                            </c:forEach>
                        </select>
                        <label id='err_team' class='error'></label>
                    </td>
                    <td align="center">
                        <select name="win">
                            <option>1.5</option>
                            <option>1.6</option>
                            <option>1.7</option>
                            <option>1.8</option>
                            <option>1.9</option>
                            <option>2.0</option>
                            <option>2.1</option>
                            <option>2.2</option>
                            <option>2.3</option>
                            <option>2.4</option>
                            <option>2.5</option>
                            <option>2.6</option>
                            <option>2.7</option>
                            <option>2.8</option>
                            <option>2.9</option>
                            <option>3.0</option>
                            <option>3.1</option>
                            <option>3.2</option>
                            <option>3.3</option>
                            <option>3.4</option>
                            <option>3.5</option>
                            <option>3.6</option>
                            <option>3.7</option>
                            <option>3.8</option>
                            <option>3.9</option>
                            <option>4.0</option>
                            <option>4.1</option>
                            <option>4.2</option>
                            <option>4.3</option>
                            <option>4.4</option>
                            <option>4.5</option>
                        </select>
                        <select name="draw">
                            <option>1.5</option>
                            <option>1.6</option>
                            <option>1.7</option>
                            <option>1.8</option>
                            <option>1.9</option>
                            <option>2.0</option>
                            <option>2.1</option>
                            <option>2.2</option>
                            <option>2.3</option>
                            <option>2.4</option>
                            <option>2.5</option>
                            <option>2.6</option>
                            <option>2.7</option>
                            <option>2.8</option>
                            <option>2.9</option>
                            <option>3.0</option>
                            <option>3.1</option>
                            <option>3.2</option>
                            <option>3.3</option>
                            <option>3.4</option>
                            <option>3.5</option>
                            <option>3.6</option>
                            <option>3.7</option>
                            <option>3.8</option>
                            <option>3.9</option>
                            <option>4.0</option>
                            <option>4.1</option>
                            <option>4.2</option>
                            <option>4.3</option>
                            <option>4.4</option>
                            <option>4.5</option>
                        </select>
                        <select name="defeat">
                            <option>1.5</option>
                            <option>1.6</option>
                            <option>1.7</option>
                            <option>1.8</option>
                            <option>1.9</option>
                            <option>2.0</option>
                            <option>2.1</option>
                            <option>2.2</option>
                            <option>2.3</option>
                            <option>2.4</option>
                            <option>2.5</option>
                            <option>2.6</option>
                            <option>2.7</option>
                            <option>2.8</option>
                            <option>2.9</option>
                            <option>3.0</option>
                            <option>3.1</option>
                            <option>3.2</option>
                            <option>3.3</option>
                            <option>3.4</option>
                            <option>3.5</option>
                            <option>3.6</option>
                            <option>3.7</option>
                            <option>3.8</option>
                            <option>3.9</option>
                            <option>4.0</option>
                            <option>4.1</option>
                            <option>4.2</option>
                            <option>4.3</option>
                            <option>4.4</option>
                            <option>4.5</option>
                        </select>
                    </td>
                </tr>

            </table>
                <div align="center" class="buttonField">
                    <button name="create" value="create"><fmt:message key="match.create2"/></button>
                </div>
                    </form>
                </li>
                </ul>
        </article>


            <div class="line"></div>




        <article id="article2">
            <h2 align="center"><fmt:message key="match.empty"/></h2>
            <div class="line"></div>
            <ul class="history">
                <li>
            <c:forEach var="match" items="${matchResult}">
                <form name="updateMatchForm" method="POST" action="/FrontController">
                    <input type="hidden" name="matchId" value="${match.getId()}">
                    <input type="hidden" name="command" value="update"/>
                <table class="match">
                    <tr align="center">
                        <th><fmt:message key="match.name"/></th>
                        <th><fmt:message key="match.time"/></th>
                        <c:forEach var="winlevel" items="${match.getWinLevelList()}">
                            <c:if test="${winlevel.getBet() eq 'WIN'}">
                                <th><fmt:message key="win"/></th>
                            </c:if>
                            <c:if test="${winlevel.getBet() eq 'DRAW'}">
                                <th><fmt:message key="draw"/></th>
                            </c:if>
                            <c:if test="${winlevel.getBet() eq 'DEFEAT'}">
                                <th><fmt:message key="defeat"/></th>
                            </c:if>
                        </c:forEach>
                        <th><fmt:message key="match.result"/></th>
                        <c:if test="${(match.getResult() eq 'EMPTY') && (match.getTime() < time)}">
                            <th><fmt:message key="match.update"/></th>
                        </c:if>
                    </tr>
                    <tr class="border_bottom" align="center">
                        <td align="left">
                            <c:forEach var="event" items="${match.getEventInfoList()}">
                                <c:if test="${event.getName() eq 'Бразилия'}">
                                    <fmt:message key="team.brazil"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Германия'}">
                                    <fmt:message key="team.germany"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Испания'}">
                                    <fmt:message key="team.spain"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Аргентина'}">
                                    <fmt:message key="team.argentina"/>
                                </c:if>
                                <c:choose>
                                    <c:when test="${event.isHome() eq 'true'}">
                                        <fmt:message key="match.home"/><br>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="match.away"/><br>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </td>
                        <td><fmt:formatDate value="${match.getTime()}" pattern="yyyy-MM-dd hh:mm"/></td>
                        <c:forEach var="winlevel" items="${match.getWinLevelList()}">
                            <td><c:out value="${winlevel.getCoefficient()}" /></td>
                        </c:forEach>
                        <c:choose>
                        <c:when test="${(match.getResult() eq 'EMPTY') && (match.getTime() < time)}">
                            <td>
                                <select name="result">
                                    <option value="EMPTY" disabled><fmt:message key="empty"/></option>
                                    <option value="WIN"><fmt:message key="win"/></option>
                                    <option value="DRAW"><fmt:message key="draw"/></option>
                                    <option value="DEFEAT"><fmt:message key="defeat"/></option>
                                </select>
                            </td>
                            <td>
                                <div align="center" class="buttonField">
                                    <button name="update" value="update"><fmt:message key="match.update2"/></button>
                                </div>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${match.getResult() eq 'WIN'}">
                                <td><fmt:message key="win"/></td>
                            </c:if>
                            <c:if test="${match.getResult() eq 'DRAW'}">
                                <td><fmt:message key="draw"/></td>
                            </c:if>
                            <c:if test="${match.getResult() eq 'DEFEAT'}">
                                <td><fmt:message key="defeat"/></td>
                            </c:if>
                            <c:if test="${match.getResult() eq 'EMPTY'}">
                                <td><fmt:message key="empty"/></td>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    </tr>
                </table>
                    </form>
            </c:forEach>
                </li>
            </ul>
        </article>

    </section>



    <footer>

        <div class="line"></div>

        <p><fmt:message key="copyright"/></p>

        <a href="#" class="up1"><fmt:message key="goup"/></a>
        <a href="#" class="up2"><fmt:message key="goup"/></a>

    </footer>
</section>
<script src="${pageContext.request.contextPath}/js/script.js?<?php echo time(); ?>"></script>
</body>
</html>