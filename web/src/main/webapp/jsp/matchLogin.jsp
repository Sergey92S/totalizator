<%--
  Created by IntelliJ IDEA.
  User: test
  Date: 10.01.2017
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>

<html lang="${language}">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><fmt:message key="totalizator.h1"/></title>

    <link rel="stylesheet" type="text/css" href="css/styles.css?<?php echo time(); ?>" />

</head>

<body>

<div class="fields">

    <label><fmt:message key="greeting"/> ${person.getName()} ${person.getSurname()}</label>
    <label><fmt:message key="info"/> ${person.getBalance()}</label>

    <form name="logoutForm" method="POST" action="FrontController">
        <input type="hidden" name="command" value="logout"/>
        <div class="fields-criteria">
            <button name="logout" value="logout"><fmt:message key="logout"/></button>
        </div>
    </form>

</div>

<section id="page">

    <header>
        <hgroup>
            <h1><fmt:message key="totalizator.h1"/></h1>
            <h3><fmt:message key="totalizator.h3"/></h3>
        </hgroup>

        <nav class="bottom">
            <ul>
                <li><a href="?command=match&competitionID=${competitionID}&language=en" ><fmt:message key="totalizator.language.en"/></a></li>
                <li><a href="?command=match&competitionID=${competitionID}&language=ru" }><fmt:message key="totalizator.language.ru"/></a></li>
            </ul>
        </nav>

        <nav class="top">
            <ul>
                <li>
                    <a href="?command=competition"><fmt:message key="match.back"/></a>
                </li>
            </ul>
        </nav>

    </header>

    <section id="articles">
        <article>
            <c:forEach var="match" items="${matchResult}">

                <table class="match">
                    <tr align="center">
                        <th><fmt:message key="match.name"/></th>
                        <th><fmt:message key="match.time"/></th>
                        <c:forEach var="winlevel" items="${match.getWinLevelList()}">
                            <c:if test="${winlevel.getBet() eq 'WIN'}">
                                <th><fmt:message key="win"/></th>
                            </c:if>
                            <c:if test="${winlevel.getBet() eq 'DRAW'}">
                                <th><fmt:message key="draw"/></th>
                            </c:if>
                            <c:if test="${winlevel.getBet() eq 'DEFEAT'}">
                                <th><fmt:message key="defeat"/></th>
                            </c:if>
                        </c:forEach>
                        <th><fmt:message key="match.result"/></th>
                    </tr>
                    <tr class="border_bottom" align="center">
                        <td align="left">
                            <c:forEach var="event" items="${match.getEventInfoList()}">
                                <c:if test="${event.getName() eq 'Бразилия'}">
                                    <fmt:message key="team.brazil"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Германия'}">
                                    <fmt:message key="team.germany"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Испания'}">
                                    <fmt:message key="team.spain"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Аргентина'}">
                                    <fmt:message key="team.argentina"/>
                                </c:if>
                                <c:choose>
                                    <c:when test="${event.isHome() eq 'true'}">
                                        <fmt:message key="match.home"/><br>
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:message key="match.away"/><br>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </td>
                        <td><fmt:formatDate value="${match.getTime()}" pattern="yyyy-MM-dd hh:mm"/></td>
                        <c:forEach var="winlevel" items="${match.getWinLevelList()}">
                            <c:forEach var="winLevelInner" items="${winLevelUser}">
                                <c:if test="${winLevelInner eq winlevel.getId()}">
                                    <c:set var="item" value="${winlevel.getId()}"></c:set>
                                </c:if>
                            </c:forEach>
                            <c:if test="${match.getTime() < time}">
                                <c:set var="item" value="${winlevel.getId()}"></c:set>
                            </c:if>
                            <c:choose>
                                <c:when test="${(item ne winlevel.getId())}">
                                    <td><a href="FrontController?command=rate&matchID=${match.getId()}&winLevelUser=${winlevel.getId()}"><c:out value="${winlevel.getCoefficient()}" /></a></td>
                                </c:when>
                                <c:otherwise>
                                    <td><c:out value="${winlevel.getCoefficient()}" /></td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${match.getResult() eq 'WIN'}">
                            <td><fmt:message key="win"/></td>
                        </c:if>
                        <c:if test="${match.getResult() eq 'DRAW'}">
                            <td><fmt:message key="draw"/></td>
                        </c:if>
                        <c:if test="${match.getResult() eq 'DEFEAT'}">
                            <td><fmt:message key="defeat"/></td>
                        </c:if>
                        <c:if test="${match.getResult() eq 'EMPTY'}">
                            <td><fmt:message key="empty"/></td>
                        </c:if>
                    </tr>
                </table>

            </c:forEach>
        </article>
    </section>



    <footer>

        <div class="line"></div>

        <p><fmt:message key="copyright"/></p>

        <a href="#" class="up1"><fmt:message key="goup"/></a>
        <a href="#" class="up2"><fmt:message key="goup"/></a>

    </footer>
</section>

</body>
</html>
