<%--
  Created by IntelliJ IDEA.
  User: test
  Date: 10.01.2017
  Time: 23:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>

<html lang="${language}">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><fmt:message key="totalizator.h1"/></title>

    <link rel="stylesheet" type="text/css" href="css/styles.css?<?php echo time(); ?>"/>

</head>

<body>

<div class="fields">

    <label><fmt:message key="greeting"/> ${person.getName()} ${person.getSurname()}</label>
    <label><fmt:message key="info"/> ${person.getBalance()}</label>

    <form onsubmit="return checkMoney()" name="addMoneyForm" method="POST" action="FrontController">
        <input type="hidden" name="command" value="updateBalance"/>
        <div class="buttonField">
            <input id="money" size="5" type="text" name="money" value=""/>
            <button name="addMoney" value="addMoney"><fmt:message key="rate.add"/></button>
        </div>
        <label id='err_money' class='error'></label>
    </form>

    <form name="logoutForm" method="POST" action="FrontController">
        <input type="hidden" name="command" value="logout"/>
        <div class="fields-criteria">
            <button name="logout" value="logout"><fmt:message key="logout"/></button>
        </div>
    </form>

</div>

<section id="page">

    <header>
        <hgroup>
            <h1><fmt:message key="totalizator.h1"/></h1>
            <h3><fmt:message key="totalizator.h3"/></h3>
        </hgroup>

        <nav class="bottom">

            <ul>
                <li><a href="?command=rate&winLevelUser=${winLevelUser}&language=en"><fmt:message
                        key="totalizator.language.en"/></a></li>
                <li><a href="?command=rate&winLevelUser=${winLevelUser}&language=ru" }><fmt:message
                        key="totalizator.language.ru"/></a></li>
            </ul>

        </nav>

        <nav class="top">
            <ul>
                <li>
                    <a href="?command=competition"><fmt:message key="match.back"/></a>
                </li>
                <li>
                    <a href="?command=match&competitionID=${competitionID}"><fmt:message key="rate.back"/></a>
                </li>
            </ul>
        </nav>

    </header>

    <section id="articles">


        <div class="line"></div>

                    <article id="article1">
                        <h2 align="center"><fmt:message key="rate.slip"/></h2>
                        <div class="line"></div>
                        <ul class="history">
                        <li>

                    <form onsubmit="return checkAmount('${person.getBalance()}')" name="matchForm" method="POST"
                          action="FrontController">
                        <input type="hidden" name="command" value="bet"/>
                        <input type="hidden" name="winLevelUser" value="${winLevel.getId()}"/>
                        <input type="hidden" name="competitionID" value="${competitionID}"/>
                        <p><fmt:message key="rate.match"/><c:out value="${winLevel.getName()}"/></p>
                        <p><fmt:message key="rate.teams"/>
                            <c:forEach var="event" items="${winLevel.getEventInfoList()}" varStatus="loop">
                                <%--<c:out value="${event.getName()}"/>--%>
                                <c:if test="${event.getName() eq 'Бразилия'}">
                                    <fmt:message key="team.brazil"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Германия'}">
                                    <fmt:message key="team.germany"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Испания'}">
                                    <fmt:message key="team.spain"/>
                                </c:if>
                                <c:if test="${event.getName() eq 'Аргентина'}">
                                    <fmt:message key="team.argentina"/>
                                </c:if>
                                <c:if test="${loop.index eq 0}">
                                    <fmt:message key="rate.versus"/>
                                </c:if>
                            </c:forEach>
                        </p>
                        <p><fmt:message key="rate.time"/><fmt:formatDate value="${winLevel.getTime()}" pattern="yyyy-MM-dd hh:mm"/></p>
                        <p><fmt:message key="rate.type"/>
                            <c:if test="${winLevel.getBet() eq 'WIN'}">
                                <fmt:message key="win"/>
                            </c:if>
                            <c:if test="${winLevel.getBet() eq 'DRAW'}">
                                <fmt:message key="draw"/>
                            </c:if>
                            <c:if test="${winLevel.getBet() eq 'DEFEAT'}">
                                <fmt:message key="defeat"/>
                            </c:if>
                        <p><fmt:message key="rate.coefficient"/><c:out value="${winLevel.getCoefficient()}"/></p>
                        <div align="center" class="buttonField">
                            <label id='err_amount' class='error'></label>
                            <input id="amount" align="center" type="text" size="10" name="amount" value=""/><fmt:message key="rate.multiplication"/><c:out
                                value="${winLevel.getCoefficient()}"/>
                            <button name="bet" value="bet"><fmt:message key="rate"/></button>
                        </div>
                    </form>

                </li>
                            </ul>
                    </article>

                <div class="line"></div>

                    <article id="article2">

                        <h2 align="center"><fmt:message key="rate.history"/></h2>
                        <div class="line"></div>
                        <ul class="history">
                        <li>
                    <c:forEach var="rate" items="${rateResult}">
                        <form name="deleteRateForm" method="POST" action="/FrontController">
                            <input type="hidden" name="matchId" value="${rate.getId()}">
                            <input type="hidden" name="command" value="delete"/>
                        <table class="match">
                            <tr align="center">
                                <th><fmt:message key="match.name"/></th>
                                <th><fmt:message key="match.time"/></th>
                                <c:if test="${rate.getBet() eq 'WIN'}">
                                    <th><fmt:message key="win"/></th>
                                </c:if>
                                <c:if test="${rate.getBet() eq 'DRAW'}">
                                    <th><fmt:message key="draw"/></th>
                                </c:if>
                                <c:if test="${rate.getBet() eq 'DEFEAT'}">
                                    <th><fmt:message key="defeat"/></th>
                                </c:if>
                                <th><fmt:message key="match.result"/></th>
                                <th><fmt:message key="rate.your"/></th>
                                <c:if test="${rate.getTime() > time}">
                                    <th><fmt:message key="rate.delete"/></th>
                                </c:if>
                            </tr>
                            <tr class="border_bottom" align="center">
                                <td align="left">
                                    <c:forEach var="event" items="${rate.getEventInfoList()}">
                                        <c:if test="${event.getName() eq 'Бразилия'}">
                                            <fmt:message key="team.brazil"/>
                                        </c:if>
                                        <c:if test="${event.getName() eq 'Германия'}">
                                            <fmt:message key="team.germany"/>
                                        </c:if>
                                        <c:if test="${event.getName() eq 'Испания'}">
                                            <fmt:message key="team.spain"/>
                                        </c:if>
                                        <c:if test="${event.getName() eq 'Аргентина'}">
                                            <fmt:message key="team.argentina"/>
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${event.isHome() eq 'true'}">
                                                <fmt:message key="match.home"/><br>
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="match.away"/><br>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </td>
                                <td><fmt:formatDate value="${rate.getTime()}" pattern="yyyy-MM-dd hh:mm"/></td>
                                <td><c:out value="${rate.getCoefficient()}" /></td>
                                <c:if test="${rate.getResult() eq 'WIN'}">
                                    <td><fmt:message key="win"/></td>
                                </c:if>
                                <c:if test="${rate.getResult() eq 'DRAW'}">
                                    <td><fmt:message key="draw"/></td>
                                </c:if>
                                <c:if test="${rate.getResult() eq 'DEFEAT'}">
                                    <td><fmt:message key="defeat"/></td>
                                </c:if>
                                <c:if test="${rate.getResult() eq 'EMPTY'}">
                                    <td><fmt:message key="empty"/></td>
                                </c:if>
                                <td><c:out value="${rate.getAmount()}" /></td>

                                    <c:if test="${rate.getTime() > time}">
                                        <td>
                                            <div align="center" class="buttonField">
                                                <button name="delete" value="delete"><fmt:message key="delete"/></button>
                                            </div>
                                        </td>
                                    </c:if>

                            </tr>
                        </table>
                        </form>
                    </c:forEach>
                        </li>
                        </ul>
                    </article>





    </section>

    <footer>

        <div class="line"></div>

        <p><fmt:message key="copyright"/></p>

        <a href="#" class="up1"><fmt:message key="goup"/></a>
        <a href="#" class="up2"><fmt:message key="goup"/></a>

    </footer>
</section>
<script src="${pageContext.request.contextPath}/js/script.js?<?php echo time(); ?>"></script>

</body>
</html>