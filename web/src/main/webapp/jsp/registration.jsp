<%--
  Created by IntelliJ IDEA.
  User: test
  Date: 10.01.2017
  Time: 23:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>

<html lang="${language}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="totalizator.h1"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css?<?php echo time(); ?>" />
</head>
<body>

<section id="page">
    <header>
        <hgroup>
            <h1><fmt:message key="totalizator.h1"/></h1>
            <h3><fmt:message key="totalizator.h3"/></h3>
        </hgroup>

        <nav class="bottom">

            <ul>
                <li><a href="?command=registration&language=en"><fmt:message
                        key="totalizator.language.en"/></a></li>
                <li><a href="?command=registration&language=ru" }><fmt:message
                        key="totalizator.language.ru"/></a></li>
            </ul>

        </nav>
    </header>
    <section id="articles">
        <div class="line"></div>
        <article>
            <h2 align="center"><fmt:message key="registration"/></h2>
            <div class="line"></div>
            <form onsubmit="return checkForm('${sessionScope.language}', '${loginList.size()}')" name="registrationForm" method="POST" action="/FrontController">
<c:forEach var="login" items="${loginList}" varStatus="loop">
    <input type="hidden" name="loginPerson" value="${login}"/>
</c:forEach>
                <input type="hidden" name="command" value="autorization"/>
                <div align="center"><p><font size="5" color="red">${errorMessage}</font></p></div>
                <ul class="credential-list">
                    <li>
                        <div class="credential-criteria">
                            <label><fmt:message key="registration.login"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="login" name="login" type="text">
                            <label id='err_login' class='error'></label>
                        </div>

                        <div class="credential-criteria">
                            <label><fmt:message key="registration.password"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="password" name="password" type="password">
                            <label id='err_password' class='error'></label>
                        </div>

                        <div class="credential-criteria">
                            <label><fmt:message key="registration.age"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="age" name="age" type="text">
                            <label id='err_age' class='error'></label>
                        </div>

                        <div class="credential-criteria">
                            <label><fmt:message key="registration.name"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="name" name="name" type="text">
                            <label id='err_name' class='error'></label>
                        </div>
                    </li>
                    <li>
                        <div class="credential-criteria">
                            <label><fmt:message key="registration.surname"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="surname" name="surname" type="text">
                            <label id='err_surname' class='error'></label>
                        </div>

                        <div class="credential-criteria">
                            <label><fmt:message key="registration.country"/></label>
                        </div>
                        <div class="credential-criteria">
                            <select name="country">
                                <option><fmt:message key="registration.country1"/></option>
                                <option><fmt:message key="registration.country2"/></option>
                                <option><fmt:message key="registration.country3"/></option>
                            </select>
                        </div>
                        <div class="credential-criteria">
                            <label><fmt:message key="registration.question"/></label>
                        </div>
                        <div class="credential-criteria">
                            <select name="question">
                                <option><fmt:message key="registration.question1"/></option>
                                <option><fmt:message key="registration.question2"/></option>
                                <option><fmt:message key="registration.question3"/></option>
                            </select>
                        </div>
                        <div class="credential-criteria">
                            <label><fmt:message key="registration.answer"/></label>
                        </div>
                        <div class="credential-criteria">
                            <input id="answer" name="answer" type="text">
                            <label id='err_answer' class='error'></label>
                        </div>
                    </li>
                    <li>
                        <div class="credential-criteria">
                            <label><fmt:message key="registration.email"/></label>
                        </div>
                        <div id="kids" class="add_kid">
                            <input id="email_1" name="email_1" type="text">
                            <button onclick="addKid(); setSubmit(this);" value="+">+</button>
                            <label id='err_email_1' class='error'></label>
                        </div>
                    </li>
                </ul>
                <div class="credential-criteria" align="center">
                    <button name="multisubmit" onclick="setSubmit(this)" value="Register"><fmt:message key="registration.register"/></button>
                    <button name="multisubmit" onclick="setSubmit(this)" value="Cancel"><fmt:message key="registration.cancel"/></button>
                </div>
            </form>
        </article>
    </section>
    <footer class="clear">
        <div class="line"></div>

        <p><fmt:message key="copyright"/></p>

        <a href="#" class="up1"><fmt:message key="goup"/></a>
        <a href="#" class="up2"><fmt:message key="goup"/></a>
    </footer>
</section>

<script src="${pageContext.request.contextPath}/js/script.js?<?php echo time(); ?>"></script>
</body>
</html>
