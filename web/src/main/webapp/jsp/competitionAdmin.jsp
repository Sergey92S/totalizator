<%--
  Created by IntelliJ IDEA.
  User: test
  Date: 08.01.2017
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>

<html lang="${language}">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title><fmt:message key="totalizator.h1"/></title>

    <link rel="stylesheet" type="text/css" href="css/styles.css?<?php echo time(); ?>" />

</head>

<body>

<div class="fields">

    <label><fmt:message key="greeting"/> ${person.getName()} ${person.getSurname()}</label>

    <form name="logoutForm" method="POST" action="FrontController">
        <input type="hidden" name="command" value="logout"/>
        <div class="fields-criteria">
            <button name="logout" value="logout"><fmt:message key="logout"/></button>
        </div>
    </form>

</div>


<section id="page">

    <c:set var="perPage" scope="session" value="${2}"/>
    <c:set var="pageStart" value="${param.start}"/>

    <c:if test="${totalCount <= pageStart}">
        <c:set var="pageStart" value="${pageStart - perPage}"/>
    </c:if>

    <c:if test="${empty pageStart or pageStart < 0}">
        <c:set var="pageStart" value="0"/>
    </c:if>

    <header>

        <hgroup>
            <h1><fmt:message key="totalizator.h1"/></h1>
            <h3><fmt:message key="totalizator.h3"/></h3>
        </hgroup>

        <nav class="bottom">

            <ul>
                <li><a href="?command=competition&start=${pageStart}&language=en" ><fmt:message key="totalizator.language.en"/></a></li>
                <li><a href="?command=competition&start=${pageStart}&language=ru" }><fmt:message key="totalizator.language.ru"/></a></li>
            </ul>

        </nav>

        <nav class="top">
            <ul>
                <c:forEach var="games" items="${result}" varStatus="loop" begin="${pageStart}"
                           end="${pageStart + perPage - 1}">
                    <li>
                        <a href="#article${loop.index+1}">
                            <c:choose>
                                <c:when test="${language eq 'en'}">
                                    ${games.key.getNameEn()}
                                </c:when>
                                <c:otherwise>
                                    ${games.key.getNameRu()}
                                </c:otherwise>
                            </c:choose>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </nav>

    </header>

    <section id="articles">
        <div class="line"></div>

        <div align="center">
            <a href="?command=competition&start=${pageStart - perPage}"><<</a>
            ${pageStart + 1} - ${pageStart + perPage}
            <a href="?command=competition&start=${pageStart + perPage}">>></a>
        </div>

        <c:forEach var="games" items="${result}" varStatus="loop" begin="${pageStart}" end="${pageStart + perPage - 1}">

            <article id="article${loop.index+1}">
                <c:choose>
                    <c:when test="${language eq 'en'}">
                        <h2 align="center">${games.key.getNameEn()}</h2>
                    </c:when>
                    <c:otherwise>
                        <h2 align="center">${games.key.getNameRu()}</h2>
                    </c:otherwise>
                </c:choose>
                <div class="line"></div>
                <ul class="events-list">
                    <c:forEach items="${games.value}" var="item" varStatus="innerLoop">
                        <li>
                            <h2>
                                <a href="FrontController?command=match&competitionID=${item.getId()}">
                                    <c:choose>
                                        <c:when test="${language eq 'en'}">
                                            <c:out value="${item.getNameEn()}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:out value="${item.getNameRu()}"/>
                                        </c:otherwise>
                                    </c:choose>
                                </a>
                            </h2>
                            <a href="FrontController?command=match&competitionID=${item.getId()}" class="event-photo"><img src=
                                                                                                                               <c:out value="${item.getImage()}"/> height="130"
                                                                                                                           alt="world_cup_foot"/></a>
                            <p>
                                <c:choose>
                                    <c:when test="${language eq 'en'}">
                                        <c:out value="${item.getDescriptionEn()}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${item.getDescriptionRu()}"/>
                                    </c:otherwise>
                                </c:choose>
                            </p>
                        </li>
                    </c:forEach>
                </ul>
            </article>
        </c:forEach>

    </section>

    <footer>

        <div class="line"></div>

        <p><fmt:message key="copyright"/></p>

        <a href="#" class="up1"><fmt:message key="goup"/></a>
        <a href="#" class="up2"><fmt:message key="goup"/></a>

    </footer>

</section>

</body>
</html>