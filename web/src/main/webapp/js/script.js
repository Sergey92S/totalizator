var i = 1;

function addKid() {
    if (i < 3) {
        i++;
        var div = document.createElement('div');
        div.innerHTML = "<input id='email_" + i + "' name='email_" + i + "' type='text'> <button class='add_kid' onclick='removeKid(this)' value='-'>-</button> <label id='err_email_" + i + "' class='error'></label> ";
        document.getElementById('kids').appendChild(div);
    }
}

function removeKid(div) {
    document.getElementById('kids').removeChild(div.parentNode);
    i--;
}

var submitName;

function setSubmit(button) {
    submitName = button.value;
}

function checkName() {
    var isTrue = true;

    if (document.getElementById('name').value == "") {
        document.getElementById('err_name').innerHTML = 'Name is required';
        document.getElementById('err_name').style.color = 'red';
        isTrue = false;
    } else {
        document.getElementById('err_name').innerHTML = "";
    }
    
    if (document.getElementById('home').value == document.getElementById('away').value) {
        document.getElementById('team_name').innerHTML = 'These teams must be different';
        document.getElementById('team_name').style.color = 'red';
        isTrue = false;
    } else {
        document.getElementById('team_name').innerHTML = "";
    }

    return isTrue;
}

function checkLoginForm() {
    var language = arguments[0];

    var LOGIN_FIELD_1 = "Login is required",
        LOGIN_FIELD_2 = "Login is not valid",
        PASSWORD_FIELD_1 = "Password is required",
        PASSWORD_FIELD_2 = "Password is not valid";

    if (language == 'ru_RU') {
        LOGIN_FIELD_1 = "Поле Логин обязательно",
        LOGIN_FIELD_2 = "Поле Логин не соответствует",
        PASSWORD_FIELD_1 = "Поле Пароль обязательно",
        PASSWORD_FIELD_2 = "Поле Пароль не соответствует";
    }

    if (submitName == "registration") {
        return true;
    }

    var loginRegEx = /^[a-zA-Z][\w]{4,20}$/;
    var passwordRegEx = /(?=^.{6,20}$)(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])/;

    var isTrue = true;

    if (document.getElementById('login').value == "") {
        if(language)
            document.getElementById('err_login').innerHTML = LOGIN_FIELD_1;
        document.getElementById('err_login').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('login').value.match(loginRegEx)) {
        document.getElementById('err_login').innerHTML = LOGIN_FIELD_2;
        document.getElementById('err_login').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_login').innerHTML = "";
    }

    if (document.getElementById('password').value == "") {
        document.getElementById('err_password').innerHTML = PASSWORD_FIELD_1;
        document.getElementById('err_password').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('password').value.match(passwordRegEx)) {
        document.getElementById('err_password').innerHTML = PASSWORD_FIELD_2;
        document.getElementById('err_password').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_password').innerHTML = "";
    }

    return isTrue;
}

function checkForm() {
    var language = arguments[0];
    var size = arguments[1];
    var loginList = document.getElementsByName('loginPerson');

    var LOGIN_FIELD_1 = "Login is required",
        LOGIN_FIELD_2 = "Login is not valid",
        LOGIN_FIELD_3 = "This Login is exist",
        PASSWORD_FIELD_1 = "Password is required",
        PASSWORD_FIELD_2 = "Password is not valid",
        NAME_FIELD_1 = "Name is required",
        NAME_FIELD_2 = "Name is not valid",
        SURNAME_FIELD_1 = "Surname is required",
        SURNAME_FIELD_2 = "Surname is not valid",
        AGE_FIELD_1 = "Age is required",
        AGE_FIELD_2 = "Age is not valid",
        SURNAME_FIELD = "Surname is required",
        EMAIL_FIELD_1 = "Email is required",
        EMAIL_FIELD_2 = "Email is not valid";

    if (language == 'ru_RU') {
        LOGIN_FIELD_1 = "Поле Логин обязательно",
        LOGIN_FIELD_2 = "Поле Логин не соответствует",
        LOGIN_FIELD_3 = "Такой логин уже есть",
        PASSWORD_FIELD_1 = "Поле Пароль обязательно",
        PASSWORD_FIELD_2 = "Поле Пароль не соответствует",
        NAME_FIELD_1 = "Поле Имя обязательно",
        NAME_FIELD_2 = "Поле Имя не соответствует",
        SURNAME_FIELD_1 = "Поле Фамилия обязательно",
        SURNAME_FIELD_2 = "Поле Фамилия не соответствует",
        AGE_FIELD_1 = "Поле Возраст обязательно",
        AGE_FIELD_2 = "Поле Возраст не соответствует",
        SURNAME_FIELD = "Поле Фамилия обязательно",
        EMAIL_FIELD_1 = "Поле Почта обязательно",
        EMAIL_FIELD_2 = "Поле Почта не соответствует";
    }

    var loginRegEx = /^[a-zA-Z][\w]{4,20}$/;

    var passwordRegEx = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[\d]).{6,20}$/;

    var ageRegEx = /^([789]|\d\d|1[01]\d|120)$/

    var emailRegEx = /^[a-z0-9]+@[a-z0-9]+\.[a-z]{2,}$/
    
    var nameRegEx = /^[A-Z][a-z]{1,15}$/
    
    var surnameRegEx = /^[A-Z][a-z]{1,15}$/

    var isTrue = true;

    if (submitName == "Cancel") {
        return true;
    }

    if (submitName == "Cancel") {
        return true;
    }

    if (submitName == "+") {
        return false;
    }

    if (document.getElementById('login').value == "") {
        if(language)
        document.getElementById('err_login').innerHTML = LOGIN_FIELD_1;
        document.getElementById('err_login').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('login').value.match(loginRegEx)) {
        document.getElementById('err_login').innerHTML = LOGIN_FIELD_2;
        document.getElementById('err_login').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_login').innerHTML = "";
    }

    for (var i = 0; i < size; i++) {
        if(document.getElementById('login').value == loginList[i].value){
            document.getElementById('err_login').innerHTML = LOGIN_FIELD_3;
            document.getElementById('err_login').style.color = 'red';
            isTrue = false;
        }
    }

    if (document.getElementById('password').value == "") {
        document.getElementById('err_password').innerHTML = PASSWORD_FIELD_1;
        document.getElementById('err_password').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('password').value.match(passwordRegEx)) {
        document.getElementById('err_password').innerHTML = PASSWORD_FIELD_2;
        document.getElementById('err_password').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_password').innerHTML = "";
    }

    if (document.getElementById('name').value == "") {
        document.getElementById('err_name').innerHTML = NAME_FIELD_1;
        document.getElementById('err_name').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('name').value.match(nameRegEx)) {
        document.getElementById('err_name').innerHTML = NAME_FIELD_2;
        document.getElementById('err_name').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_name').innerHTML = "";
    }

    if (document.getElementById('surname').value == "") {
        document.getElementById('err_surname').innerHTML = SURNAME_FIELD_1;
        document.getElementById('err_surname').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('surname').value.match(surnameRegEx)) {
        document.getElementById('err_surname').innerHTML = SURNAME_FIELD_2;
        document.getElementById('err_surname').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_surname').innerHTML = "";
    }

    if (document.getElementById('age').value == "") {
        document.getElementById('err_age').innerHTML = AGE_FIELD_1;
        document.getElementById('err_age').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('age').value.match(ageRegEx)) {
        document.getElementById('err_age').innerHTML = AGE_FIELD_2;
        document.getElementById('err_age').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_age').innerHTML = "";
    }
    if (document.getElementById('answer').value == "") {
        document.getElementById('err_answer').innerHTML = SURNAME_FIELD;
        document.getElementById('err_answer').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_answer').innerHTML = "";
    }

    if (document.getElementById('email_1').value == "") {
        document.getElementById('err_email_1').innerHTML = EMAIL_FIELD_1;
        document.getElementById('err_email_1').style.color = 'red';
        isTrue = false;
    } else if (!document.getElementById('email_1').value.match(emailRegEx)) {
        document.getElementById('err_email_1').innerHTML = EMAIL_FIELD_2;
        document.getElementById('err_email_1').style.color = 'red';
        isTrue = false;
    }
    else {
        document.getElementById('err_email_1').innerHTML = "";
    }

    if (document.getElementById('email_2') !== null) {
        if (document.getElementById('email_2').value == "") {
            document.getElementById('err_email_2').innerHTML = EMAIL_FIELD_1;
            document.getElementById('err_email_2').style.color = 'red';
            isTrue = false;
        } else if (!document.getElementById('email_2').value.match(emailRegEx)) {
            document.getElementById('err_email_2').innerHTML = EMAIL_FIELD_2;
            document.getElementById('err_email_2').style.color = 'red';
            isTrue = false;
        }
        else {
            document.getElementById('err_email_2').innerHTML = "";
        }
    }

    if (document.getElementById('email_3') !== null) {
        if (document.getElementById('email_3').value == "") {
            document.getElementById('err_email_3').innerHTML = EMAIL_FIELD_1;
            document.getElementById('err_email_3').style.color = 'red';
            isTrue = false;
        } else if (!document.getElementById('email_3').value.match(emailRegEx)) {
            document.getElementById('err_email_3').innerHTML = EMAIL_FIELD_2;
            document.getElementById('err_email_3').style.color = 'red';
            isTrue = false;
        }
        else {
            document.getElementById('err_email_3').innerHTML = "";
        }
    }

    return isTrue;
}

function checkAmount() {
    var balance = arguments[0];
    var amountRegEx = /^[+]?[\d]*|^[+]?[\d]*\.?[\d]+$/;
    var zero = 0;
    var amount = +document.getElementById('amount').value;

    if (document.getElementById('amount').value == "") {
        document.getElementById('err_amount').innerHTML = 'Amount is required';
        document.getElementById('err_amount').style.color = 'red';
        return false;
    }
    else if(!document.getElementById('amount').value.match(amountRegEx)){
        document.getElementById('err_amount').innerHTML = 'Amount does not have any letters';
        document.getElementById('err_amount').style.color = 'red';
        return false;
    }
    else if ((amount > balance) || (amount < zero)) {
        document.getElementById('err_amount').innerHTML = 'Amount is not valid';
        document.getElementById('err_amount').style.color = 'red';
        return false;
    }
    return true;
}

function checkMoney(){
    var moneyRegEx = /^[+]?[\d]*|^[+]?[\d]*\.?[\d]+$/;
    var money = +document.getElementById('money').value;
    var maxAmount = 10000;

    if (document.getElementById('money').value == "") {
        document.getElementById('err_money').innerHTML = 'Amount is required';
        document.getElementById('err_money').style.color = 'red';
        return false;
    }
    else if((!document.getElementById('money').value.match(moneyRegEx)) || (money > maxAmount)){
        document.getElementById('err_money').innerHTML = 'Amount is not valid';
        document.getElementById('err_money').style.color = 'red';
        return false;
    }
    return true;
}