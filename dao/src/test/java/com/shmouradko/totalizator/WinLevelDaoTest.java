package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.WinLevelDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class WinLevelDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        WinLevelDao winLevelDao = WinLevelDao.getInstance();
        assertTrue(winLevelDao.getAllWinLevels(1) != null);
    }

}