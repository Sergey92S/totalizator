package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.MatchDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class MatchDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        MatchDao matchDao = MatchDao.getInstance();
        assertTrue(matchDao.getAllMatches(1) != null);
        assertTrue(matchDao.find(1) != null);
    }

}
