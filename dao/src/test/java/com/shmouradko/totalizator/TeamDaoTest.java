package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.TeamDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class TeamDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        TeamDao teamDao = TeamDao.getInstance();
        assertTrue(teamDao.find(1) != null);
    }

}