package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.RateDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class RateDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        RateDao rateDao = RateDao.getInstance();
        assertTrue(rateDao.getAllUserRates(1) != null);
        assertTrue(rateDao.getAllUserWinLevelId(1) != null);
    }

}

