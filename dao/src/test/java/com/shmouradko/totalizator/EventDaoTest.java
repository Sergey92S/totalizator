package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.EventDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class EventDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        EventDao eventDao = EventDao.getInstance();
        assertTrue(eventDao.getAllEvents(1) != null);
    }

}