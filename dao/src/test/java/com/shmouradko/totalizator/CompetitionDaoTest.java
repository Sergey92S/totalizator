package com.shmouradko.totalizator;

import com.shmouradko.totalizator.dao.impl.CompetitionDao;
import com.shmouradko.totalizator.exception.DaoException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by test on 17.01.2017.
 */
public class CompetitionDaoTest extends TestCase {

    @Test
    public void test() throws DaoException {
        CompetitionDao competitionDao = CompetitionDao.getInstance();
        assertTrue(competitionDao.getAllCompetitions(1) != null);
    }

}
