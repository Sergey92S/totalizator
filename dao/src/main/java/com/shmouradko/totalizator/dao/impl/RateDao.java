package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.IRateDao;
import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.Rate;
import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.viewobject.RateInfo;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Revotech on 11.01.2017.
 */
public class RateDao extends BaseDao<Rate> implements IRateDao {
    public static RateDao instance;
    private static final String SQL_SELECT_WIN_LEVEL_ID = "SELECT WinLevel_ID FROM rate WHERE rate.Person_ID = ?";
    private static final String SQL_SELECT_USER_RATE = "SELECT totalizator.match.id, totalizator.match.time, winlevel.coefficient, winlevel.type, totalizator.match.result, rate.amount FROM totalizator.match, winlevel, rate WHERE (rate.Person_ID = ?) AND (winlevel.ID = rate.WinLevel_ID) AND (totalizator.match.ID = winlevel.Match_ID)";
    private static final String SQL_SAVE_RATE = "INSERT INTO Rate (Amount, WinLevel_ID, Person_ID) VALUES (?, ?, ?)";
    private static final String SQL_DELETE_RATE = "DELETE Rate FROM Rate, WinLevel WHERE (WinLevel.Match_ID = ?) AND (WinLevel.ID = Rate.WinLevel_ID) AND (Rate.Person_ID = ?)";
    private static Logger LOGGER = LogManager.getLogger(RateDao.class);

    private RateDao() {
        super();
    }

    public static RateDao getInstance() {
        if (instance == null) {
            instance = new RateDao();
        }
        return instance;
    }

    @Override
    public Rate prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareSaveOperation(Rate rate, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SAVE_RATE)) {
            st.setDouble(1, rate.getAmount());
            st.setLong(2, rate.getWinLevelId());
            st.setLong(3, rate.getPersonId());
            st.executeUpdate();
        }
    }

    @Override
    public void prepareUpdateOperation(Rate entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public List<Long> getAllUserWinLevelId(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<Long> idList;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_WIN_LEVEL_ID)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                idList = new ArrayList<>();
                while (res.next()) {
                    idList.add(res.getLong(1));
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return idList;
    }

    @Override
    public void deleteRate(long matchId, long personId) throws DaoException{
        ConnectionWrapper connection = null;
        try {
            connection = POOL.takeConnection();
            connection.setAutoCommit(false);
            try(PreparedStatement st = connection.prepareStatement(SQL_DELETE_RATE)) {
                st.setLong(1, matchId);
                st.setLong(2, personId);
                st.executeUpdate();
                connection.commit();
            }
        } catch (SQLException | InterruptedException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DaoException(e1);
            }
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
    }

    @Override
    public List<RateInfo> getAllUserRates(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<RateInfo> rateInfoList;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_USER_RATE)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                rateInfoList = new ArrayList<>();
                RateInfo rateInfo;
                while (res.next()) {
                    rateInfo = new RateInfo();
                    rateInfo.setId(res.getLong(1));
                    rateInfo.setTime(res.getTimestamp(2));
                    rateInfo.setCoefficient(res.getFloat(3));
                    rateInfo.setBet(WinLevel.BetType.valueOf(res.getString(4)));
                    rateInfo.setResult(Match.ResultType.valueOf(res.getString(5)));
                    rateInfo.setAmount(res.getDouble(6));
                    rateInfoList.add(rateInfo);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return rateInfoList;
    }

}
