package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.IEventDao;
import com.shmouradko.totalizator.entity.Event;
import com.shmouradko.totalizator.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 09.01.2017.
 */
public class EventDao extends BaseDao<Event> implements IEventDao {
    public static EventDao instance;
    private static final String SQL_EVENT = "SELECT id, home FROM event WHERE event.Match_ID = ?";
    private static final String SQL_SAVE_EVENT = "INSERT INTO event (home, Match_ID, Team_ID) VALUES (?, ?, ?)";
    private static Logger LOGGER = LogManager.getLogger(EventDao.class);

    private EventDao() {
        super();
    }

    public static EventDao getInstance() {
        if (instance == null) {
            instance = new EventDao();
        }
        return instance;
    }

    @Override
    public Event prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareSaveOperation(Event event, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SAVE_EVENT)) {
            st.setInt(1, event.isHome() ? 1 : 0);
            st.setLong(2, event.getMatchId());
            st.setLong(3, event.getTeamId());
            st.executeUpdate();
        }
    }

    @Override
    public void prepareUpdateOperation(Event entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public List<Event> getAllEvents(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<Event> events;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_EVENT)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                events = new ArrayList<>();
                Event event;
                while (res.next()) {
                    event = new Event();
                    event.setId(res.getLong(1));
                    event.setHome(res.getBoolean(2));
                    events.add(event);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return events;
    }

}
