package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.exception.DaoException;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IMatchDao {

    /**
     * check if this match is in the DB
     * @param match Match's name
     * @return result of checking
     * @throws DaoException if SQLException is thrown
     */
    boolean isMatchExist(String match) throws DaoException;

    /**
     * get all matches
     * @param id Competition id
     * @return List of matches
     * @throws DaoException if SQLException is thrown
     */
    List<Match> getAllMatches(long id) throws DaoException;

    /**
     * find Match's Id by name
     * @param name Match's name
     * @return Match's Id
     * @throws DaoException if SQLException is thrown
     */
    long findMatchIdByName(String name) throws DaoException;

}
