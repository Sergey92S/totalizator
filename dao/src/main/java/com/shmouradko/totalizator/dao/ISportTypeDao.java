package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.SportType;
import com.shmouradko.totalizator.exception.DaoException;

import java.util.List;

/**
 * Created by test on 30.01.2017.
 */
public interface ISportTypeDao {

    /**
     * get all sport types
     * @return List of sport types
     * @throws DaoException if SQLException is thrown
     */
    List<SportType> getAllSportTypes() throws DaoException;
}
