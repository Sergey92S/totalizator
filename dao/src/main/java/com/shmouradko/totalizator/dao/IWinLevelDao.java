package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.viewobject.WinLevelInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IWinLevelDao {

    /**
     * find all Win Levels
     * @param id Match's Id
     * @return Win Levels
     * @throws DaoException if SQLException is thrown
     */
    List<WinLevel> getAllWinLevels(long id) throws DaoException;

    /**
     * find Win Level Information
     * @param id Win Level's Id
     * @return Win Level Information
     * @throws DaoException if SQLException is thrown
     */
    WinLevelInfo getWinLevelInfo(long id) throws DaoException;

}
