package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.Component;
import com.shmouradko.totalizator.exception.DaoException;

/**
 * Created by test on 20.01.2017.
 */
public interface Dao<T extends Component> {

    void saveOrUpdate(T entity) throws DaoException;

    T find(long id) throws DaoException;

}
