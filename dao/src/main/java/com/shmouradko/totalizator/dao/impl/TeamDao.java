package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.ITeamDao;
import com.shmouradko.totalizator.entity.Team;
import com.shmouradko.totalizator.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Revotech on 10.01.2017.
 */
public class TeamDao extends BaseDao<Team> implements ITeamDao{
    public static TeamDao instance;
    private static final String SQL_SELECT_TEAM = "SELECT team.id, team.name, team.Competition_ID FROM team, event WHERE (event.ID = ?) AND (event.Team_ID = team.ID)";
    private static final String SQL_SELECT_ALL_TEAMS = "SELECT team.id, team.name, team.Competition_ID FROM team WHERE (team.Competition_ID = ?)";
    private static final String SQL_SELECT_TEAM_ID = "SELECT id FROM team WHERE team.name = ?";
    private static Logger LOGGER = LogManager.getLogger(TeamDao.class);

    private TeamDao() {
        super();
    }

    public static TeamDao getInstance() {
        if (instance == null) {
            instance = new TeamDao();
        }
        return instance;
    }

    @Override
    public Team prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_TEAM)) {
            st.setLong(1, id);
            ResultSet res = st.executeQuery();
            Team team = null;
            if (res.next()) {
                team = new Team();
                team.setId(res.getLong(1));
                team.setName(res.getString(2));
                team.setCompetitionId(res.getLong(3));
            }
            return team;
        }
    }

    @Override
    public void prepareSaveOperation(Team entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareUpdateOperation(Team entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public long findTeamIdByName(String name) throws DaoException{
        ConnectionWrapper connection = null;
        long id = 0;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_TEAM_ID)) {
                st.setString(1, name);
                ResultSet res = st.executeQuery();
                while (res.next()) {
                    id = res.getLong(1);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return id;
    }

    @Override
    public List<Team> getAllTeams(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<Team> teamList;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_TEAMS)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                teamList = new ArrayList<>();
                while (res.next()) {
                    Team team = new Team();
                    team.setId(res.getLong(1));
                    team.setName(res.getString(2));
                    team.setCompetitionId(res.getLong(3));
                    teamList.add(team);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return teamList;
    }
}
