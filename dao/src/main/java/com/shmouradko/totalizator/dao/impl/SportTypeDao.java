package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.ISportTypeDao;
import com.shmouradko.totalizator.entity.SportType;
import com.shmouradko.totalizator.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 30.01.2017.
 */
public class SportTypeDao extends BaseDao<SportType> implements ISportTypeDao {
    public static SportTypeDao instance;
    private static Logger LOGGER = LogManager.getLogger(SportTypeDao.class);

    private static final String SQL_SPORT_TYPE = "SELECT id, Name_En, Name_Ru, description FROM sporttype";

    private SportTypeDao() {
        super();
    }

    public static SportTypeDao getInstance() {
        if (instance == null) {
            instance = new SportTypeDao();
        }
        return instance;
    }

    @Override
    public SportType prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareSaveOperation(SportType entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareUpdateOperation(SportType entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public List<SportType> getAllSportTypes() throws DaoException {
        ConnectionWrapper connection = null;
        List<SportType> sportTypeList;
        try {
            connection = POOL.takeConnection();
            try(Statement st = connection.createStatement()) {
                ResultSet res = st.executeQuery(SQL_SPORT_TYPE);
                sportTypeList = new ArrayList<>();
                SportType sportType;
                while (res.next()) {
                    sportType = new SportType();
                    sportType.setId(res.getLong(1));
                    sportType.setNameEn(res.getString(2));
                    sportType.setNameRu(res.getString(3));
                    sportType.setDescription(res.getString(4));
                    sportTypeList.add(sportType);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return sportTypeList;
    }

}