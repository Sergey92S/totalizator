package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.IMatchDao;
import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.exception.DaoException;

import java.sql.*;
import java.util.*;

/**
 * Created by test on 09.01.2017.
 */
public class MatchDao extends BaseDao<Match> implements IMatchDao {
    public static MatchDao instance;
    private static  final String SQL_UPDATE_RESULT = "UPDATE totalizator.match SET totalizator.match.result = ? WHERE totalizator.match.ID = ?";
    private static final String SQL_SELECT_ALL_MATCHES = "SELECT id, name, time, result, Competition_ID FROM totalizator.match WHERE totalizator.match.Competition_ID = ?";
    private static final String SQL_SELECT_MATCH = "SELECT id, name, time, result, Competition_ID FROM totalizator.match WHERE totalizator.match.ID = ?";
    private static final String SQL_SELECT_MATCH_ID = "SELECT id FROM totalizator.match WHERE totalizator.match.name = ?";
    private static final String SQL_SAVE_MATCH = "INSERT INTO totalizator.match (name, time, result, Competition_ID) VALUES (?, ?, ?, ?)";
    private static final String SQL_SELECT_MATCH_NAME = "SELECT id FROM totalizator.match WHERE totalizator.match.name = ?";

    private MatchDao() {
        super();
    }

    public static MatchDao getInstance() {
        if (instance == null) {
            instance = new MatchDao();
        }
        return instance;
    }

    @Override
    public Match prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_MATCH)) {
            st.setLong(1, id);
            ResultSet res = st.executeQuery();
            Match match = null;
            if (res.next()) {
                match = new Match();
                match.setId(res.getLong(1));
                match.setName(res.getString(2));
                match.setTime(res.getTimestamp(3));
                match.setResult(Match.ResultType.valueOf(res.getString(4)));
                match.setCompetitionId(res.getLong(5));
            }
            return match;
        }
    }

    @Override
    public void prepareSaveOperation(Match match, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SAVE_MATCH)) {
            st.setString(1, match.getName());
            st.setTimestamp(2, new Timestamp(match.getTime().getTime()));
            st.setString(3, match.getResult().toString());
            st.setLong(4, match.getCompetitionId());
            st.executeUpdate();
        }
    }

    @Override
    public void prepareUpdateOperation(Match match, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_UPDATE_RESULT)) {
            st.setString(1, match.getResult().toString());
            st.setLong(2, match.getId());
            st.executeUpdate();
        }
    }

    @Override
    public long findMatchIdByName(String name) throws DaoException{
        ConnectionWrapper connection = null;
        long id = 0;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_MATCH_ID)) {
                st.setString(1, name);
                ResultSet res = st.executeQuery();
                while (res.next()) {
                    id = res.getLong(1);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return id;
    }

    @Override
    public boolean isMatchExist(String match) throws DaoException{
        ConnectionWrapper connection = null;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_MATCH_NAME)) {
                st.setString(1, match);
                ResultSet res = st.executeQuery();
                if (res.next()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
    }

    @Override
    public List<Match> getAllMatches(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<Match> matches;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_MATCHES)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                matches = new ArrayList<>();
                Match match;
                while (res.next()) {
                    match = new Match();
                    match.setId(res.getLong(1));
                    match.setName(res.getString(2));
                    match.setTime(res.getTimestamp(3));
                    match.setResult(Match.ResultType.valueOf(res.getString(4)));
                    match.setCompetitionId(res.getLong(5));
                    matches.add(match);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return matches;
    }
}
