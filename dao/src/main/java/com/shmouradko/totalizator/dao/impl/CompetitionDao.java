package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.ICompetitionDao;
import com.shmouradko.totalizator.entity.Competition;
import com.shmouradko.totalizator.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Revotech on 13.12.2016.
 */
public class CompetitionDao extends BaseDao<Competition> implements ICompetitionDao {
    public static CompetitionDao instance;
    private static Logger LOGGER = LogManager.getLogger(CompetitionDao.class);

    private static final String SQL_COMPETITION = "SELECT id, Name_En, Name_Ru, image, Description_En, Description_Ru FROM competition WHERE competition.SportType_ID = ?";

    private CompetitionDao() {
        super();
    }

    public static CompetitionDao getInstance() {
        if (instance == null) {
            instance = new CompetitionDao();
        }
        return instance;
    }

    @Override
    public Competition prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareSaveOperation(Competition entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareUpdateOperation(Competition entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public List<Competition> getAllCompetitions(long id) throws DaoException {
        ConnectionWrapper connection = null;
        List<Competition> competitionList;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_COMPETITION)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                competitionList = new ArrayList<>();
                Competition competition;
                while (res.next()) {
                    competition = new Competition();
                    competition.setId(res.getLong(1));
                    competition.setNameEn(res.getString(2));
                    competition.setNameRu(res.getString(3));
                    competition.setImage(res.getString(4));
                    competition.setDescriptionEn(res.getString(5));
                    competition.setDescriptionRu(res.getString(6));
                    competitionList.add(competition);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return competitionList;
    }

}
