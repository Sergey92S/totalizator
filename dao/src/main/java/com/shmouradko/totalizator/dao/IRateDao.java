package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.viewobject.RateInfo;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IRateDao {

    /**
     * find all Person's Win Level Id
     * @param id Person's Id
     * @return Person's Win Level Id
     * @throws DaoException if SQLException is thrown
     */
    List<Long> getAllUserWinLevelId(long id) throws DaoException;

    /**
     * find all Person's Rates
     * @param id Person's Id
     * @return Person's Rates
     * @throws DaoException if SQLException is thrown
     */
    List<RateInfo> getAllUserRates(long id) throws DaoException;

    /**
     * delete rate
     * @param matchId Match's Id
     * @param personId Person's Id
     * @throws DaoException if SQLException is thrown
     */
    void deleteRate(long matchId, long personId) throws DaoException;

}
