package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.IWinLevelDao;
import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.WinLevel;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.viewobject.WinLevelInfo;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 09.01.2017.
 */
public class WinLevelDao extends BaseDao<WinLevel> implements IWinLevelDao {
    public static WinLevelDao instance;
    private static final String SQL_SELECT_WIN_LEVEL = "SELECT id, coefficient, type FROM winlevel WHERE winlevel.Match_ID = ?";
    private static final String SQL_SELECT_WIN_LEVEL_INFO = "SELECT winlevel.id, totalizator.match.id, totalizator.match.name, totalizator.match.time, totalizator.match.result, winlevel.coefficient, winlevel.type FROM totalizator.match, winlevel WHERE (winlevel.ID = ?) AND (totalizator.match.ID = winlevel.Match_ID)";
    private static final String SQL_SAVE_WIN_LEVEL = "INSERT INTO winlevel (coefficient, type, Match_ID) VALUES (?, ?, ?)";
    private static Logger LOGGER = LogManager.getLogger(WinLevelDao.class);

    private WinLevelDao() {
        super();
    }

    public static WinLevelDao getInstance() {
        if (instance == null) {
            instance = new WinLevelDao();
        }
        return instance;
    }

    @Override
    public WinLevel prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public void prepareSaveOperation(WinLevel winLevel, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SAVE_WIN_LEVEL)) {
            st.setFloat(1, winLevel.getCoefficient());
            st.setString(2, winLevel.getBet().toString());
            st.setLong(3, winLevel.getMatchId());
            st.executeUpdate();
        }
    }

    @Override
    public void prepareUpdateOperation(WinLevel entity, ConnectionWrapper connection) throws SQLException {
        LOGGER.log(Level.FATAL, "Unused Method");
        throw new RuntimeException();
    }

    @Override
    public List<WinLevel> getAllWinLevels(long id) throws DaoException{
        ConnectionWrapper connection = null;
        List<WinLevel> winLevelList;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_WIN_LEVEL)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                winLevelList = new ArrayList<>();
                WinLevel winLevel;
                while (res.next()) {
                    winLevel = new WinLevel();
                    winLevel.setId(res.getLong(1));
                    winLevel.setCoefficient(res.getFloat(2));
                    winLevel.setBet(WinLevel.BetType.valueOf(res.getString(3)));
                    winLevelList.add(winLevel);
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return winLevelList;
    }

    @Override
    public WinLevelInfo getWinLevelInfo(long id) throws DaoException{
        ConnectionWrapper connection = null;
        WinLevelInfo winLevelInfo = null;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_WIN_LEVEL_INFO)) {
                st.setLong(1, id);
                ResultSet res = st.executeQuery();
                if (res.next()) {
                    winLevelInfo = new WinLevelInfo();
                    winLevelInfo.setId(res.getLong(1));
                    winLevelInfo.setMatchId(res.getLong(2));
                    winLevelInfo.setName(res.getString(3));
                    winLevelInfo.setTime(res.getTimestamp(4));
                    winLevelInfo.setResult(Match.ResultType.valueOf(res.getString(5)));
                    winLevelInfo.setCoefficient(res.getFloat(6));
                    winLevelInfo.setBet(WinLevel.BetType.valueOf(res.getString(7)));
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return winLevelInfo;
    }

}
