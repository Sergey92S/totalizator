package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.IPersonDao;
import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.exception.DaoException;
import com.shmouradko.totalizator.util.Coder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by test on 19.01.2017.
 */
public class PersonDao extends BaseDao<Person> implements IPersonDao{
    public static PersonDao instance;
    private static  final String SQL_UPDATE_BALANCE = "UPDATE person SET person.balance = ? WHERE person.ID = ?";
    private static final String SQL_SELECT_PERSON = "SELECT id, name, surname, login, password, country, secret_question, secret_answer, email, age, role, balance FROM person WHERE (person.ID = ?)";
    private static final String SQL_SAVE_BALANCE = "INSERT INTO person (name, surname, login, password, country, secret_question, secret_answer, email, age, role, balance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_PERSON_BY_CREDENTIALS = "SELECT id, name, surname, login, password, country, secret_question, secret_answer, email, age, role, balance FROM person WHERE (person.login = ?) AND (person.password = ?)";
    private static  final String SQL_SELECT_PERSON_LOGIN = "SELECT id FROM person WHERE person.login = ?";
    private static final String SQL_SELECT_ALL_UPDATE_PERSON_ID = "SELECT Rate.Person_ID, WinLevel.coefficient, Rate.amount FROM WinLevel, Rate WHERE (WinLevel.Match_ID = ?) AND (WinLevel.type = ?) AND (Rate.WinLevel_ID = WinLevel.ID)";
    private static final String SQL_SELECT_ALL_PERSON_LOGIN = "SELECT login FROM totalizator.person";

    private PersonDao() {
        super();
    }

    public static PersonDao getInstance() {
        if (instance == null) {
            instance = new PersonDao();
        }
        return instance;
    }

    @Override
    public Person prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_PERSON)) {
            st.setLong(1, id);
            ResultSet res = st.executeQuery();
            Person person = null;
            if (res.next()) {
                person = new Person();
                person.setId(res.getLong(1));
                person.setName(res.getString(2));
                person.setSurname(res.getString(3));
                person.setLogin(res.getString(4));
                person.setPassword(res.getString(5));
                person.setCountry(res.getString(6));
                person.setSecretAnswer(res.getString(8));
                person.setSecretQuestion(res.getString(7));
                person.setEmail(res.getString(9));
                person.setAge(res.getInt(10));
                person.setRole(res.getInt(11));
                person.setBalance(res.getDouble(12));
            }
            return person;
        }
    }

    @Override
    public void prepareSaveOperation(Person person, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_SAVE_BALANCE)) {
            st.setString(1, person.getName());
            st.setString(2, person.getSurname());
            st.setString(3, person.getLogin());
            st.setString(4, Coder.defineHashCode(person.getPassword()));
            st.setString(5, person.getCountry());
            st.setString(6, person.getSecretQuestion());
            st.setString(7, person.getSecretAnswer());
            st.setString(8, person.getEmail());
            st.setInt(9, person.getAge());
            st.setInt(10, person.getRole());
            st.setDouble(11, person.getBalance());
            st.executeUpdate();
        }
    }

    @Override
    public void prepareUpdateOperation(Person person, ConnectionWrapper connection) throws SQLException {
        try(PreparedStatement st = connection.prepareStatement(SQL_UPDATE_BALANCE)) {
            st.setDouble(1, person.getBalance());
            st.setLong(2, person.getId());
            st.executeUpdate();
        }
    }

    @Override
    public Map<Long, Float> findAllUpdatePersons(long matchId, String result) throws DaoException{
        ConnectionWrapper connection = null;
        Map<Long, Float> personData = null;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_ALL_UPDATE_PERSON_ID)) {
                st.setLong(1, matchId);
                st.setString(2, result);
                ResultSet res = st.executeQuery();
                personData = new HashMap<>();
                while (res.next()) {
                    personData.put(res.getLong(1), res.getFloat(2) * res.getFloat(3));
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {

                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return personData;
    }

    @Override
    public List<String> findAllPersonLogin() throws DaoException{
        ConnectionWrapper connection = null;
        List<String> personNameList = null;
        try {
            connection = POOL.takeConnection();
            try(Statement st = connection.createStatement()) {
                ResultSet res = st.executeQuery(SQL_SELECT_ALL_PERSON_LOGIN);
                personNameList = new ArrayList<>();
                while (res.next()) {
                    personNameList.add(res.getString(1));
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {

                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return personNameList;
    }

    @Override
    public boolean isLoginExist(String login) throws DaoException{
        ConnectionWrapper connection = null;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_PERSON_LOGIN)) {
                st.setString(1, login);
                ResultSet res = st.executeQuery();
                if (res.next()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
    }

    @Override
    public Person findPersonByCredentials(String login, String password) throws DaoException{
        ConnectionWrapper connection = null;
        Person person = null;
        try {
            connection = POOL.takeConnection();
            try(PreparedStatement st = connection.prepareStatement(SQL_SELECT_PERSON_BY_CREDENTIALS)){
            st.setString(1, login);
            st.setString(2, Coder.defineHashCode(password));
            st.setString(2, password);
            ResultSet res = st.executeQuery();
            if (res.next()) {
                person = new Person();
                person.setId(res.getLong(1));
                person.setName(res.getString(2));
                person.setSurname(res.getString(3));
                person.setLogin(res.getString(4));
                person.setPassword(res.getString(5));
                person.setCountry(res.getString(6));
                person.setSecretQuestion(res.getString(7));
                person.setSecretAnswer(res.getString(8));
                person.setEmail(res.getString(9));
                person.setAge(res.getInt(10));
                person.setRole(res.getInt(11));
                person.setBalance(res.getDouble(12));
            }
            }
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
        return person;
    }

}
