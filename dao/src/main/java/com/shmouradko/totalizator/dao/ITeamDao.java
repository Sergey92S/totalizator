package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.Team;
import com.shmouradko.totalizator.exception.DaoException;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface ITeamDao {

    /**
     * find Team Id by it's name
     * @param name Team's Id
     * @return Team Id
     * @throws DaoException if SQLException is thrown
     */
    long findTeamIdByName(String name) throws DaoException;

    /**
     * find all Teams
     * @param id Competition's Id
     * @return Teams
     * @throws DaoException if SQLException is thrown
     */
    List<Team> getAllTeams(long id) throws DaoException;

}
