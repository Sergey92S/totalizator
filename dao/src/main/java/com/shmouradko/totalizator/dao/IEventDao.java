package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.Event;
import com.shmouradko.totalizator.exception.DaoException;

import java.util.List;

/**
 * Created by test on 17.01.2017.
 */
public interface IEventDao {

    /**
     * get all events
     * @param id Match id
     * @return List of events
     * @throws DaoException if SQLException is thrown
     */
    List<Event> getAllEvents(long id) throws DaoException;

}
