package com.shmouradko.totalizator.dao;

import com.shmouradko.totalizator.entity.Person;
import com.shmouradko.totalizator.exception.DaoException;

import java.util.List;
import java.util.Map;

/**
 * Created by test on 24.01.2017.
 */
public interface IPersonDao {

    /**
     * find Person by login and password
     * @param login Person's login
     * @param password Person's password
     * @return Person
     * @throws DaoException if SQLException is thrown
     */
    Person findPersonByCredentials(String login, String password) throws DaoException;

    /**
     * check if such login is in the DB
     * @param login Person's login
     * @return result of checking
     * @throws DaoException if SQLException is thrown
     */
    boolean isLoginExist(String login) throws DaoException;

    /**
     * find all Person's login
     * @return Person's logins
     * @throws DaoException if SQLException is thrown
     */
    List<String> findAllPersonLogin() throws DaoException;

    /**
     * find all persons who won with rate
     * @param matchId Match's Id
     * @param result Game result
     * @return map with Person's Id and Balance
     * @throws DaoException if SQLException is thrown
     */
    Map<Long, Float> findAllUpdatePersons(long matchId, String result) throws DaoException;
}
