package com.shmouradko.totalizator.dao.impl;

import com.shmouradko.totalizator.connection.ConnectionPool;
import com.shmouradko.totalizator.connection.ConnectionWrapper;
import com.shmouradko.totalizator.dao.Dao;
import com.shmouradko.totalizator.entity.Component;
import com.shmouradko.totalizator.exception.DaoException;

import java.sql.SQLException;

/**
 * Created by test on 20.01.2017.
 */
public abstract class BaseDao<T extends Component> implements Dao<T> {
    protected static final ConnectionPool POOL = ConnectionPool.getInstance();
    private static final int EMPTY_ID = 0;

    public BaseDao(){

    }

    @Override
    public void saveOrUpdate(T entity) throws DaoException{
        ConnectionWrapper connection = null;
        try {
            connection = POOL.takeConnection();
            connection.setAutoCommit(false);
            if (entity.getId()==EMPTY_ID){
                prepareSaveOperation(entity, connection);
            } else {
                prepareUpdateOperation(entity, connection);
            }
            connection.commit();
        } catch (SQLException | InterruptedException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                throw new DaoException(e1);
            }
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
    }

    @Override
    public T find(long id) throws DaoException {
        ConnectionWrapper connection = null;
        try{
            connection = POOL.takeConnection();
            return prepareFindOperation(id, connection);
        } catch (SQLException | InterruptedException e) {
            throw new DaoException(e);
        } finally {
            try {
                POOL.returnConnection(connection);
            } catch (SQLException | InterruptedException e) {
                throw new DaoException(e);
            }
        }
    }

    public abstract T prepareFindOperation(long id, ConnectionWrapper connection) throws SQLException;

    public abstract void prepareSaveOperation(T entity, ConnectionWrapper connection) throws SQLException;

    public abstract void prepareUpdateOperation(T entity, ConnectionWrapper connection) throws SQLException;

}
