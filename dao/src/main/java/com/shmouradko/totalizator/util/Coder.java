package com.shmouradko.totalizator.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by test on 25.01.2017.
 */
public class Coder {
    private static String ALGORITHM = "MD5";
    private static int POSITIVE = 1;
    private static String FORMAT = "%032x";

    public static String defineHashCode(String password){
        try {
            MessageDigest dig = MessageDigest.getInstance(ALGORITHM);
            BigInteger hash = new BigInteger(POSITIVE, dig.digest(password.getBytes()));
            return String.format(FORMAT, hash);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
