package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 09.01.2017.
 */
public abstract class Component {
    private long id;

    public Component(){

    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
