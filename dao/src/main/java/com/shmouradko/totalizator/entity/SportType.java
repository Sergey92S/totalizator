package com.shmouradko.totalizator.entity;

/**
 * Created by test on 08.01.2017.
 */
public class SportType extends Component implements Comparable<SportType> {
    private String nameEn;
    private String nameRu;
    private String description;

    public SportType(){

    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(SportType sportType) {
        return nameEn.compareTo(sportType.getNameEn());
    }
}
