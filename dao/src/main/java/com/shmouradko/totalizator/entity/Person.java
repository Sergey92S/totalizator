package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 05.01.2017.
 */
public class Person extends Component{
    private String name;
    private String surname;
    private String login;
    private String password;
    private String country;
    private String secretQuestion;
    private String secretAnswer;
    private String email;
    private int age;
    private int role;
    private double balance;

    public Person(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (role != person.role) return false;
        if (Double.compare(person.balance, balance) != 0) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;
        if (login != null ? !login.equals(person.login) : person.login != null) return false;
        if (password != null ? !password.equals(person.password) : person.password != null) return false;
        if (country != null ? !country.equals(person.country) : person.country != null) return false;
        if (secretQuestion != null ? !secretQuestion.equals(person.secretQuestion) : person.secretQuestion != null)
            return false;
        if (secretAnswer != null ? !secretAnswer.equals(person.secretAnswer) : person.secretAnswer != null)
            return false;
        return email != null ? email.equals(person.email) : person.email == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (secretQuestion != null ? secretQuestion.hashCode() : 0);
        result = 31 * result + (secretAnswer != null ? secretAnswer.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + role;
        temp = Double.doubleToLongBits(balance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", country='" + country + '\'' +
                ", secretQuestion='" + secretQuestion + '\'' +
                ", secretAnswer='" + secretAnswer + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", role=" + role +
                ", balance=" + balance +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getCountry() {
        return country;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public String getEmail() {
        return email;
    }

    public int getRole() {
        return role;
    }

    public int getAge() {
        return age;
    }

    public double getBalance() {
        return balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
