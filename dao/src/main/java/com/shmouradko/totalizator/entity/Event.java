package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 09.01.2017.
 */
public class Event extends Component {
    private boolean home;
    private long matchId;
    private long teamId;

    public Event(){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (home != event.home) return false;
        if (matchId != event.matchId) return false;
        return teamId == event.teamId;

    }

    @Override
    public int hashCode() {
        int result = (home ? 1 : 0);
        result = 31 * result + (int) (matchId ^ (matchId >>> 32));
        result = 31 * result + (int) (teamId ^ (teamId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "home=" + home +
                ", matchId=" + matchId +
                ", teamId=" + teamId +
                '}';
    }

    public void setHome(boolean home) {
        this.home = home;
    }

    public boolean isHome() {
        return home;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }
}
