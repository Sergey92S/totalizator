package com.shmouradko.totalizator.entity;

import java.util.Date;

/**
 * Created by Revotech on 09.01.2017.
 */
public class Match extends Component{
    private String name;
    private Date time;
    private ResultType result;
    private long competitionId;

    public enum ResultType {
        WIN, DEFEAT, DRAW, EMPTY
    }

    public Match(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (competitionId != match.competitionId) return false;
        if (name != null ? !name.equals(match.name) : match.name != null) return false;
        if (time != null ? !time.equals(match.time) : match.time != null) return false;
        return result == match.result;

    }

    @Override
    public int hashCode() {
        int result1 = name != null ? name.hashCode() : 0;
        result1 = 31 * result1 + (time != null ? time.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (int) (competitionId ^ (competitionId >>> 32));
        return result1;
    }

    @Override
    public String toString() {
        return "Match{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", result=" + result +
                ", competitionId=" + competitionId +
                '}';
    }

    public String getName() {
        return name;
    }

    public Date getTime() {
        return time;
    }

    public ResultType getResult() {
        return result;
    }

    public void setResult(ResultType result) {
        this.result = result;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public long getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(long competitionId) {
        this.competitionId = competitionId;
    }
}
