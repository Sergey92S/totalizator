package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 09.01.2017.
 */
public class WinLevel extends Component{
    private float coefficient;
    private BetType bet;
    private long matchId;

    public enum BetType {
        WIN, DEFEAT, DRAW
    }

    public WinLevel(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WinLevel winLevel = (WinLevel) o;

        if (Float.compare(winLevel.coefficient, coefficient) != 0) return false;
        if (matchId != winLevel.matchId) return false;
        return bet == winLevel.bet;

    }

    @Override
    public int hashCode() {
        int result = (coefficient != +0.0f ? Float.floatToIntBits(coefficient) : 0);
        result = 31 * result + (bet != null ? bet.hashCode() : 0);
        result = 31 * result + (int) (matchId ^ (matchId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "WinLevel{" +
                "coefficient=" + coefficient +
                ", bet=" + bet +
                ", matchId=" + matchId +
                '}';
    }

    public float getCoefficient() {
        return coefficient;
    }

    public BetType getBet() {
        return bet;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    public void setBet(BetType bet) {
        this.bet = bet;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }
}
