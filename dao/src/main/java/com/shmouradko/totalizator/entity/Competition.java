package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 13.12.2016.
 */
public class Competition extends Component {
    private String nameEn;
    private String nameRu;
    private String image;
    private String descriptionEn;
    private String descriptionRu;

    public Competition(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Competition that = (Competition) o;

        if (nameEn != null ? !nameEn.equals(that.nameEn) : that.nameEn != null) return false;
        if (nameRu != null ? !nameRu.equals(that.nameRu) : that.nameRu != null) return false;
        if (image != null ? !image.equals(that.image) : that.image != null) return false;
        if (descriptionEn != null ? !descriptionEn.equals(that.descriptionEn) : that.descriptionEn != null)
            return false;
        return descriptionRu != null ? descriptionRu.equals(that.descriptionRu) : that.descriptionRu == null;

    }

    @Override
    public int hashCode() {
        int result = nameEn != null ? nameEn.hashCode() : 0;
        result = 31 * result + (nameRu != null ? nameRu.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (descriptionEn != null ? descriptionEn.hashCode() : 0);
        result = 31 * result + (descriptionRu != null ? descriptionRu.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Competition{" +
                "nameEn='" + nameEn + '\'' +
                ", nameRu='" + nameRu + '\'' +
                ", image='" + image + '\'' +
                ", descriptionEn='" + descriptionEn + '\'' +
                ", descriptionRu='" + descriptionRu + '\'' +
                '}';
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
