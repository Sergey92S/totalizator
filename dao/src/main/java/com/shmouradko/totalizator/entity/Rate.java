package com.shmouradko.totalizator.entity;

/**
 * Created by Revotech on 11.01.2017.
 */
public class Rate extends Component {
    private double amount;
    private long personId;
    private long winLevelId;

    public Rate() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rate rate = (Rate) o;

        if (Double.compare(rate.amount, amount) != 0) return false;
        if (personId != rate.personId) return false;
        return winLevelId == rate.winLevelId;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(amount);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (personId ^ (personId >>> 32));
        result = 31 * result + (int) (winLevelId ^ (winLevelId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "amount=" + amount +
                ", personId=" + personId +
                ", winLevelId=" + winLevelId +
                '}';
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public long getWinLevelId() {
        return winLevelId;
    }

    public void setWinLevelId(long winLevelId) {
        this.winLevelId = winLevelId;
    }
}
