package com.shmouradko.totalizator.viewobject;

import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.WinLevel;

import java.util.Date;
import java.util.List;

/**
 * Created by Revotech on 12.01.2017.
 */
public class RateInfo {
    private List<EventInfo> eventInfoList;
    private Date time;
    private float coefficient;
    private WinLevel.BetType bet;
    private Match.ResultType result;
    private double amount;
    private long id;

    public RateInfo(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RateInfo rateInfo = (RateInfo) o;

        if (Float.compare(rateInfo.coefficient, coefficient) != 0) return false;
        if (Double.compare(rateInfo.amount, amount) != 0) return false;
        if (id != rateInfo.id) return false;
        if (eventInfoList != null ? !eventInfoList.equals(rateInfo.eventInfoList) : rateInfo.eventInfoList != null)
            return false;
        if (time != null ? !time.equals(rateInfo.time) : rateInfo.time != null) return false;
        if (bet != rateInfo.bet) return false;
        return result == rateInfo.result;

    }

    @Override
    public int hashCode() {
        int result1;
        long temp;
        result1 = eventInfoList != null ? eventInfoList.hashCode() : 0;
        result1 = 31 * result1 + (time != null ? time.hashCode() : 0);
        result1 = 31 * result1 + (coefficient != +0.0f ? Float.floatToIntBits(coefficient) : 0);
        result1 = 31 * result1 + (bet != null ? bet.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        temp = Double.doubleToLongBits(amount);
        result1 = 31 * result1 + (int) (temp ^ (temp >>> 32));
        result1 = 31 * result1 + (int) (id ^ (id >>> 32));
        return result1;
    }

    @Override
    public String toString() {
        return "RateInfo{" +
                "eventInfoList=" + eventInfoList +
                ", time=" + time +
                ", coefficient=" + coefficient +
                ", bet=" + bet +
                ", result=" + result +
                ", amount=" + amount +
                ", id=" + id +
                '}';
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setEventInfoList(List<EventInfo> eventInfoList) {
        this.eventInfoList = eventInfoList;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    public void setBet(WinLevel.BetType bet) {
        this.bet = bet;
    }

    public void setResult(Match.ResultType result) {
        this.result = result;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public List<EventInfo> getEventInfoList() {
        return eventInfoList;
    }

    public Date getTime() {
        return time;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public WinLevel.BetType getBet() {
        return bet;
    }

    public Match.ResultType getResult() {
        return result;
    }

    public double getAmount() {
        return amount;
    }
}
