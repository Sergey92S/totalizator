package com.shmouradko.totalizator.viewobject;

import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.WinLevel;

import java.util.Date;
import java.util.List;

/**
 * Created by Revotech on 10.01.2017.
 */
public class WinLevelInfo {
    private long id;
    private long matchId;
    private String name;
    private Date time;
    private Match.ResultType result;
    private List<EventInfo> eventInfoList;
    private float coefficient;
    private WinLevel.BetType bet;

    public WinLevelInfo(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WinLevelInfo that = (WinLevelInfo) o;

        if (id != that.id) return false;
        if (matchId != that.matchId) return false;
        if (Float.compare(that.coefficient, coefficient) != 0) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (result != that.result) return false;
        if (eventInfoList != null ? !eventInfoList.equals(that.eventInfoList) : that.eventInfoList != null)
            return false;
        return bet == that.bet;

    }

    @Override
    public int hashCode() {
        int result1 = (int) (id ^ (id >>> 32));
        result1 = 31 * result1 + (int) (matchId ^ (matchId >>> 32));
        result1 = 31 * result1 + (name != null ? name.hashCode() : 0);
        result1 = 31 * result1 + (time != null ? time.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (eventInfoList != null ? eventInfoList.hashCode() : 0);
        result1 = 31 * result1 + (coefficient != +0.0f ? Float.floatToIntBits(coefficient) : 0);
        result1 = 31 * result1 + (bet != null ? bet.hashCode() : 0);
        return result1;
    }

    @Override
    public String toString() {
        return "WinLevelInfo{" +
                "id=" + id +
                ", matchId=" + matchId +
                ", name='" + name + '\'' +
                ", time=" + time +
                ", result=" + result +
                ", eventInfoList=" + eventInfoList +
                ", coefficient=" + coefficient +
                ", bet=" + bet +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMatchId() {
        return matchId;
    }

    public void setMatchId(long matchId) {
        this.matchId = matchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Match.ResultType getResult() {
        return result;
    }

    public void setResult(Match.ResultType result) {
        this.result = result;
    }

    public List<EventInfo> getEventInfoList() {
        return eventInfoList;
    }

    public void setEventInfoList(List<EventInfo> eventInfoList) {
        this.eventInfoList = eventInfoList;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    public WinLevel.BetType getBet() {
        return bet;
    }

    public void setBet(WinLevel.BetType bet) {
        this.bet = bet;
    }
}
