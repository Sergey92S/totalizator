package com.shmouradko.totalizator.viewobject;

/**
 * Created by test on 09.01.2017.
 */
public class EventInfo {
    private String name;
    private boolean home;

    public EventInfo(){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventInfo eventInfo = (EventInfo) o;

        if (home != eventInfo.home) return false;
        return name != null ? name.equals(eventInfo.name) : eventInfo.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (home ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventInfo{" +
                "name='" + name + '\'' +
                ", home=" + home +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHome(boolean home) {
        this.home = home;
    }

    public String getName() {
        return name;
    }

    public boolean isHome() {
        return home;
    }
}
