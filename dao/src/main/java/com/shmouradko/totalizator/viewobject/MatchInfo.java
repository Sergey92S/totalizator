package com.shmouradko.totalizator.viewobject;

import com.shmouradko.totalizator.entity.Match;
import com.shmouradko.totalizator.entity.WinLevel;

import java.util.Date;
import java.util.List;

/**
 * Created by test on 09.01.2017.
 */
public class MatchInfo {
    private long id;
    private String name;
    private Date time;
    private Match.ResultType result;
    private List<EventInfo> eventInfoList;
    private List<WinLevel> winLevelList;

    public MatchInfo(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchInfo matchInfo = (MatchInfo) o;

        if (id != matchInfo.id) return false;
        if (name != null ? !name.equals(matchInfo.name) : matchInfo.name != null) return false;
        if (time != null ? !time.equals(matchInfo.time) : matchInfo.time != null) return false;
        if (result != matchInfo.result) return false;
        if (eventInfoList != null ? !eventInfoList.equals(matchInfo.eventInfoList) : matchInfo.eventInfoList != null)
            return false;
        return winLevelList != null ? winLevelList.equals(matchInfo.winLevelList) : matchInfo.winLevelList == null;

    }

    @Override
    public int hashCode() {
        int result1 = (int) (id ^ (id >>> 32));
        result1 = 31 * result1 + (name != null ? name.hashCode() : 0);
        result1 = 31 * result1 + (time != null ? time.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (eventInfoList != null ? eventInfoList.hashCode() : 0);
        result1 = 31 * result1 + (winLevelList != null ? winLevelList.hashCode() : 0);
        return result1;
    }

    @Override
    public String toString() {
        return "MatchInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", time=" + time +
                ", result=" + result +
                ", eventInfoList=" + eventInfoList +
                ", winLevelList=" + winLevelList +
                '}';
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getTime() {
        return time;
    }

    public Match.ResultType getResult() {
        return result;
    }

    public List<EventInfo> getEventInfoList() {
        return eventInfoList;
    }

    public List<WinLevel> getWinLevelList() {
        return winLevelList;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setResult(Match.ResultType result) {
        this.result = result;
    }

    public void setEventInfoList(List<EventInfo> eventInfoList) {
        this.eventInfoList = eventInfoList;
    }

    public void setWinLevelList(List<WinLevel> winLevelList) {
        this.winLevelList = winLevelList;
    }
}
